import React from "react";
import { View, StyleSheet} from "react-native";
import { Button } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";


const CompletableButton = ({ text, loading, onPress, completed }) => {

  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      <Button
        mode={"contained"}
        loading={loading}
        compact={true}
        uppercase={false}
        icon={() => <Icon name={"done"} size={24} color={"#08BEE5"} />}
        color={"#FFFFFF"}
        style={{ elevation: completed ? 0 : 4 }}
        onPress={() => {
          onPress?.();
        }}
        disabled={completed}
      >{text}</Button>

      {
        completed && (
          <View style={styles.iconBg}>
            <Icon name={"done"} color={"white"} size={16} />
          </View>
        )
      }
    </View>
  );

};

const styles = StyleSheet.create({
  iconBg: {
    height: 24,
    width: 24,
    padding: 4,
    backgroundColor: "#2CC68F",
    borderRadius: 12,
    marginStart: 12,
  }
});

export default CompletableButton;
