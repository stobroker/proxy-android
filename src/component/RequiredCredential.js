import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const RequiredCredential = ({ name, description, isProvided }) => {

  return (
    <View>
      <View style={{
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 8,
      }}>

        <View style={{ flex: 1 }}>
          <View style={{ backgroundColor: "#32FFF5", width: 32, height: 32, borderRadius: 6 }} />
        </View>

        <View style={{ flex: 10, marginStart: 16 }}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.description}>{description}</Text>
        </View>

        <View style={{ flex: 1 }}>
          {isProvided ? (<Icon name={"done"} color={"white"} size={16} style={{
              height: 24,
              width: 24,
              padding: 4,
              backgroundColor: "#2CC68F",
              borderRadius: 12,
            }} />)
            : (<></>)}
        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  name: {
    color: "#212B36",
    fontSize: 14,
  },
  description: {
    color: "#8FA4B5",
    fontSize: 12,
  },
});

export default RequiredCredential;
