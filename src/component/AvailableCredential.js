import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const AvailableCredential = ({ name, isRequired, onAddPress }) => {

  return (
    <View>
      <View style={{
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 8,
      }}>

        <View style={{ flex: 1 }}>
          <View style={{ backgroundColor: "#32FFF5", width: 32, height: 32, borderRadius: 6 }} />
        </View>

        <View style={{ flex: 10, marginStart: 16 }}>
          <Text style={styles.name}>{name}</Text>
        </View>

        <View style={{ flex: 1 }}>
          <TouchableOpacity onPress={() => onAddPress?.()}>
            <Icon name={"add"} color={"#8FA4B5"} size={24} style={{
              height: 32,
              width: 32,
              padding: 4,
              elevation: 4,
              backgroundColor: "#FFFFFF",
              borderRadius: 4,
            }} />
          </TouchableOpacity>
        </View>

      </View>

      {isRequired && (<Text style={{ color: "#08BEE5", marginStart: 48 }}>Required</Text>)}
    </View>
  );
};

const styles = StyleSheet.create({
  name: {
    color: "#212B36",
    fontSize: 14,
  },
  description: {
    color: "#8FA4B5",
    fontSize: 12,
  },
});

export default AvailableCredential;
