import React from "react";
import { Text, View } from "react-native";

const IssueStatus = ({status}) =>  {

  let text, background, textColor = null;

  switch (status) {
    case "COMPLETE": {
      text = "COMPLETE"
      background = "#DDE4E9"
      textColor = "#8FA4B5"
      break;
    }
    case "FOR_REVIEW": {
      text = "FOR REVIEW"
      background = "#95ECFF"
      textColor = "#08BEE5"
      break;
    }
    case "SIGNED": {
      text = "SIGNED"
      background = "#95ECFF"
      textColor = "#08BEE5"
      break;
    }
    case "WITH_ISSUE": {
      text = "ISSUE"
      background = "#95ECFF"
      textColor = "#08BEE5"
      break;
    }
    default: {
      throw "Unsupported status " + status
    }
  }

  return (
    <View style={{
      backgroundColor: background,
      paddingHorizontal: 6,
      paddingVertical: 2,
      borderRadius: 16
    }}>
      <Text style={{
        color: textColor,
        fontSize: 12
      }}>{text}</Text>
    </View>
  )
};

export default IssueStatus
