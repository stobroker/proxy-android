import React from "react";
import { StyleSheet, Text, View } from "react-native";

const CredentialProperty = ({ label, valueComponent, valueText }) => {
  return (
    <View style={styles.property}>
      <View style={styles.leftColumn}>
        <Text style={styles.propertyLabel}>{label}</Text>
      </View>
      <View style={styles.rightColumn}>
        {
          valueComponent != null ? valueComponent : (<Text style={styles.propertyValue}>{valueText}</Text>)
        }
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  propertyLabel: {
    color: "#637381",
  },

  propertyValue: {
    color: "#212B36",
  },

  leftColumn: {
    flex: 1,
    alignItems: "flex-start",
  },

  rightColumn: {
    flex: 1,
    alignItems: "flex-end",
  },

  property: {
    marginVertical: 8,
    flexDirection: "row",
  },
});

export default CredentialProperty;
