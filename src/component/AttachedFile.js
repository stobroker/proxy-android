import React from 'react'
import Icon from "react-native-vector-icons/MaterialIcons";
import { StyleSheet, View, Text } from "react-native";

const AttachedFile = ({name}) => {

  return (
    <View style={{flexDirection: "row", alignItems: "center"}}>
      <Icon name={"attachment"}
      color={"#8FA4B5"}
      style={styles.icon}
      size={20}/>
      <Text numberOfLines={1} ellipsizeMode='middle' style={styles.name}>{name}</Text>
    </View>
  )
};

const styles = StyleSheet.create({

  name: {
    flex: 1,
    color: "#08BEE5",
    marginStart: 8
  },

  icon: {
    transform: [{ rotate: '315deg'}]
  }

});

export default AttachedFile;
