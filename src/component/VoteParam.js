import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'
import { ProgressBar } from "react-native-paper";

const VoteParam = ({title ="", progress= 0, need = 0}) => {

  return (
    <View style={styles.container}>
      <View style={{flexDirection: "row", paddingHorizontal: 24, paddingVertical: 8}}>
        <Text style={styles.containerTitle}>{title}</Text>
        <TouchableOpacity style={{marginStart: 12}}>
          <View style={{backgroundColor: "#C8D7EA", width: 16, height: 16, borderRadius: 8, justifyContent:"center", alignItems: "center"}}>
            <Text style={{color:"#8FA4B5", fontSize: 12}}>?</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={[styles.topBorderContainer, {paddingHorizontal:24, paddingVertical: 24}]}>
        <View style={{flexDirection: "row"}}>
          <Text style={{color: '#212B36'}}>{progress}%</Text>
          <Text style={{color: '#90979a', marginStart: 8}}>({need}% needed)</Text>
        </View>

        <View>
          <ProgressBar
            color={"#2CC68F"}
            visible={true}
            progress={progress / 100}
            style={{marginTop: 16}}
          />
        </View>
      </View>


    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 8
  },

  topBorderContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },
  containerTitle: {
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },
})

export default VoteParam;
