import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { ProgressBar } from "react-native-paper";

const VotesBlock = ({ style, titleStyle, votingObject = {participants: 1, yes: 0, no: 0, abstain:0}}) => {

  function getVoicesPercent(votes) {
    return Math.round(votes/votingObject.participants) * 100
  }

  return (
    <View style={style}>
      <Text style={titleStyle || styles.title}>Votes</Text>

      <ProgressBar
        color={"#2CC68F"}
        visible={true}
        progress={(votingObject.yes + votingObject.no + votingObject.abstain) / votingObject.participants}
        style={styles.progressStyle}
      />

      <View style={{ flexDirection: "row" }}>

        <View style={{ flex: 6 }}>
          <View style={styles.statRow}>
            <View style={styles.dotCol}>
              <View style={[styles.voteDot, styles.yes]} />
            </View>
            <View style={styles.labelCol}>
              <Text style={styles.statVotesLabel}>Yes</Text>
            </View>
            <View style={styles.percentCol}>
              <Text style={styles.statVotesPercent}>{getVoicesPercent(votingObject.yes) }%</Text>
            </View>
          </View>
          <View style={styles.statRow}>
            <View style={styles.dotCol}>
              <View style={[styles.voteDot, styles.abstain]} />
            </View>
            <View style={styles.labelCol}>
              <Text style={styles.statVotesLabel}>Abstain</Text>
            </View>
            <View style={styles.percentCol}>
              <Text style={styles.statVotesPercent}>{getVoicesPercent(votingObject.abstain)} %</Text>
            </View>
          </View>
          <View style={styles.statRow}>
            <View style={styles.dotCol}>
              <View style={[styles.voteDot, styles.no]} />
            </View>
            <View style={styles.labelCol}>
              <Text style={styles.statVotesLabel}>No</Text>
            </View>
            <View style={styles.percentCol}>
              <Text style={styles.statVotesPercent}>{getVoicesPercent(votingObject.no)} %</Text>
            </View>
          </View>
        </View>
        <View style={{ flex: 6 }} />
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    color: "#637381",
    fontSize: 14,
    textTransform: "uppercase",
  },

  progressStyle: { marginVertical: 12 },

  statVotesLabel: {
    color: "#637381",
  },

  statVotesPercent: {
    color: "#637381",
  },

  statRow: {
    marginVertical: 4,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },

  voteDot: {
    height: 8,
    width: 8,
    borderRadius: 4,
  },

  yes: {
    backgroundColor: "#2CC68F",
  },

  abstain: {
    backgroundColor: "#F5A623",
  },

  no: {
    backgroundColor: "#FF6969",
  },

  dotCol: {
    flex: 2,
  },

  labelCol: {
    flex: 6,
  },

  percentCol: {
    flex: 4,
  },
});

export default VotesBlock;
