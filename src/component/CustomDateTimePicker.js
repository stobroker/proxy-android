import React, {useState} from "react";
import { Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import DateTimePicker from '@react-native-community/datetimepicker';
import dateFormat from 'dateformat';

const CustomDateTimePicker = ({hint, style, onDateChanged}) => {

  const [date, setDate] = useState(null);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate, mode) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    onDateChanged(currentDate)
    if (mode === "date") {
      showTimepicker()
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };


  return (
   <TouchableOpacity onPress={showDatepicker}>
     <View style={[styles.input, style]}>
       <Text style={[styles.text, date != null ? styles.value : styles.hint]}>
         {date != null ? dateFormat(date, "yyyy.mm.dd HH:MM") : hint}
       </Text>
       <Icon name="calendar-today" size={18} color={"#8FA4B5"} />
     </View>

     {show && (
       <DateTimePicker
         testID="dateTimePicker"
         value={date || new Date()}
         mode={mode}
         is24Hour={true}
         display="default"
         onChange={(event, selectedDate) => onChange(event, selectedDate, mode)}
       />
     )}
   </TouchableOpacity>
  )

};

const styles = StyleSheet.create({

  input: {
    height: 40,
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection:"row",
    paddingHorizontal: 12,
  },

  text: {
    flex:5
  },

  hint: {
    color: "#8FA4B5"
  },

  value: {
    color: "#212B36"
  },

  icon: {
    flex: 1,
  }


})

export default CustomDateTimePicker;
