import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Avatar} from "react-native-paper";
import CredentialProperty from "./CredentialProperty";
import CompletableButton from "./CompletableButton";
import dateFormat from "dateformat";

const IdCredential = ({
                        credential,
                        button = {
                          onPress: () => {
                          },
                          loading: false,
                          completed: false,
                          text: ""
                        },
                        btnVisible = true
                      }) => {

  const user = credential.user;
  const label = `${user.firstName[0]}${user.lastName[0]}`;
  const properties = [
    {
      name: "Date of birth",
      value: dateFormat(new Date(credential.documentJSON.credentialSubject.data.dateOfBirth), "yyyy-mm-dd")
    },
    {name: "Citizenship", value: credential.documentJSON.credentialSubject.data.citizenship},
    {name: "Document Number", value: credential.documentJSON.credentialSubject.data.documentNumber},
    {name: "Personal code", value: credential.documentJSON.credentialSubject.data.personalDate},
    {
      name: "Date of expiry",
      value: dateFormat(new Date(credential.documentJSON.credentialSubject.data.expirationDate), "yyyy-mm-dd")
    },
  ]
  return (
    <View style={styles.container}>

      <Text style={styles.title}>ID Credential</Text>

      <View style={styles.userContainer}>
        <View style={{flex: 1, marginHorizontal: 24, marginVertical: 16}}>
          <Avatar.Text label={label} size={56}/>
        </View>
        <View style={{flex: 5}}>
          <Text style={{fontSize: 18}}>{`${user.firstName} ${user.lastName}`}</Text>
          <Text style={{fontSize: 12, color: "#637381"}}>{credential.documentJSON.credentialSubject.data.citizenship}</Text>
        </View>
      </View>

      <View style={styles.propertiesContainer}>
        {properties.map(p => <CredentialProperty label={p.name} valueText={p.value}/>)}
      </View>

      {btnVisible && (
        <View style={styles.propertiesContainer}>
          <CompletableButton
            text={button.text}
            loading={button.loading}
            completed={button.completed}
            onPress={() => {
              button.onPress()
            }}
          />
        </View>
      )}
    </View>
  );

};

const styles = StyleSheet.create({
  container: {
    marginVertical: 16,
    marginHorizontal: 16,
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
  },

  title: {
    marginHorizontal: 16,
    marginVertical: 4,
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },

  userContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    flexDirection: "row",
    alignItems: "center",
  },

  propertiesContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    paddingHorizontal: 16,
    paddingVertical: 16,
  },

});

export default IdCredential;
