import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const Countdown = ({endDate, style}) => {
  const [time, setTime] = useState({
    seconds: 0,
    minutes: 0,
    hours: 0,
    days: 0,
  })

  useEffect(()=> {
    setInterval(updateTimer, 1000)
  }, [])


  const getTimeLeft = () => {
    if (endDate != null) {
      const date = new Date(endDate);
      const until = (date.getTime() - Date.now() ) / 1000
      return {
        seconds: parseInt(until % 60),
        minutes: parseInt(until / 60, 10) % 60,
        hours: parseInt(until / (60 * 60), 10) % 24,
        days: parseInt(until / (60 * 60 * 24), 10),
      };

    } else  {
      return {
        seconds: 0,
        minutes: 0,
        hours: 0,
        days: 0,
      };
    }
  };

  function updateTimer() {
    setTime(getTimeLeft())
  }

  function format(value) {
    if (value < 10) return "0"+value
    else return value + ""
  }
  return (
    <View
      style={[{
        flexDirection: "row",
        alignItems: "center"
      }, style]}
    >
      <Icon name={"schedule"} size={16} color={"#8FA4B5"} style={{marginEnd: 12}}/>
      <Text style={styles.primaryText}>{format(time?.days)}</Text>
      <Text style={styles.secondaryText}> D </Text>
      <Text style={styles.primaryText}>{format(time?.hours)}</Text>
      <Text style={styles.secondaryText}> H : </Text>
      <Text style={styles.primaryText}>{format(time?.minutes)}</Text>
      <Text style={styles.secondaryText}> M :  </Text>
      <Text style={styles.primaryText}>{format(time?.seconds)}</Text>
      <Text style={styles.secondaryText}> S </Text>
    </View>
  )

};

const styles = StyleSheet.create({
  primaryText: {
    color: "#212B36"
  },
  secondaryText: {
    color: "#637381"
  }
})

export default Countdown
