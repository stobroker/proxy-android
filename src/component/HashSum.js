import React from "react";
import { Text, View } from "react-native";

const HashSum = ({hash}) => {

  let str = ""

  if (hash.length >= 10) {
    str = `${hash.substr(0,6)}...${hash.substr(hash.length -4, 4)}`
  } else {
    str = hash
  }
  return (
    <View style={{
      backgroundColor: "#9CF7F3",
      paddingHorizontal:8,
      paddingVertical: 2,
      borderRadius: 4
    }}>
      <Text>{str}</Text>
    </View>
  )
}

export default HashSum;
