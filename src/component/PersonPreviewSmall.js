import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Avatar } from "react-native-paper";

const defPhotoSize = 24;

const PersonPreviewSmall = ({ firstName, lastName, photo, photoStyle, labelStyle }) => {

  function getImage() {
    if (photo != null) return (
      <Image
        source={photo}
        style={[{ height: defPhotoSize, width: defPhotoSize }, photoStyle]}
      />
    );
    else {
      let first = firstName != null && firstName.length > 0 ? firstName[0] : "N";
      let last = lastName != null && lastName.length > 0 ? lastName[0] : "N";
      return (
        <Avatar.Text label={first + last} size={defPhotoSize} style={photoStyle} />
      );
    }
  }

  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      {getImage()}
      <Text style={[styles.labelStyle, labelStyle || {}]}>{`${firstName} ${lastName}`}</Text>
    </View>
  );

};

const styles = StyleSheet.create({
  image: {
    width: 24,
    height: 24,
  },

  labelStyle: {
    marginStart: 12,
    fontSize: 14,
  },
});

export default PersonPreviewSmall;
