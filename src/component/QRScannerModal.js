import React from 'react'
import {Modal} from "react-native";
import {RNCamera} from "react-native-camera";
import QRCodeScanner from "react-native-qrcode-scanner";


const QRScannerModal = ({visible, onRequestClose, onResult}) => {

  return (

    <Modal
      animationType="slide"
      transparent={false}
      visible={visible}
      onRequestClose={() => {
        onRequestClose()
      }}
    >

      <QRCodeScanner
        onRead={(e) => {
          onResult(e.data)
        }}
        flashMode={RNCamera.Constants.FlashMode.off}
      />

    </Modal>
  )
}

export default QRScannerModal;
