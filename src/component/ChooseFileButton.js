import React, {useState} from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Text } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import AttachedFile from "./AttachedFile";
import DocumentPicker from 'react-native-document-picker';
import {DocumentDirectoryPath} from "react-native-fs";


const ChooseFileButton = ({filename, onChanged, style }) => {

  function onDeleteClick() {
    onChanged(null)
  }

  async function onPickClick() {
    // Pick a single file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
        copyTo: "cachesDirectory"
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
      onChanged(res)
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  const notPicked = (
    <View style={{ flexDirection: "row", ...style }}>

      <Button
        mode={"contained"}
        uppercase={false}
        color={"#FFFFFF"}
        style={styles.chooseFileBtn}
        icon={() => (<Icon name="add" color={"#8FA4B5"} size={16} />)}
        onPress={() => {
          onPickClick();
        }}
      >
        <Text style={{ letterSpacing: 0.2 }}>Choose file </Text><Text style={{ color: "#08BEE5" }}>*</Text>
      </Button>
    </View>
  );

  const picked = (
    <View style={{ flexDirection: "row", ...style  }}>

      <View style={{flex: 2}}>
        <AttachedFile name={filename} />
      </View>

      <TouchableOpacity style={{ marginStart: 24, flex: 1 }} onPress={()=> onDeleteClick()}>
        <View style={styles.iconButton}>
          <Icon name="delete" color={"#8FA4B5"} size={24} />
        </View>
      </TouchableOpacity>
    </View>
  );


  return filename != null ? picked : notPicked;

};

const styles = StyleSheet.create({
  chooseFileBtn: {
    elevation: 4,
    marginTop: 8,
  },

  iconButton: {
    height: 32,
    width: 32,
    backgroundColor: "#FFFFFF",
    elevation: 4,
    borderRadius: 4,
    padding: 4,
  },
});

export default ChooseFileButton;
