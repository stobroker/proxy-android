import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Avatar} from "react-native-paper";
import CredentialProperty from "./CredentialProperty";
import CompletableButton from "./CompletableButton";

const EmploymentCredential = ({
                           credential,
                           button ={
                             onPress: () => {},
                             loading: false,
                             completed: false,
                             text: ""
                           },
                           btnVisible = true
                         }) => {

  const user = credential.user;
  const label = `${user.firstName[0]}${user.lastName[0]}`;
  const properties = [
    {name: "Employer", value: credential.documentJSON.credentialSubject.data.employer},
    {name: "Position", value: credential.documentJSON.credentialSubject.data.position},
    // {name: "Start date", value: cred.documentJSON.credentialSubject.data.startDate},
    // {name: "Start date", value: dateFormat(new Date(cred.documentJSON.credentialSubject.data.startDate), "yyyy-mm-dd")}
  ]
  return (
    <View style={styles.container}>

      <Text style={styles.title}>Employment Credential</Text>

      <View style={styles.userContainer}>
        <View style={{flex: 1, marginHorizontal: 24, marginVertical: 16}}>
          <Avatar.Text label={label} size={56}/>
        </View>
        <View style={{flex: 5}}>
          <Text style={{fontSize: 18}}>{`${user.firstName} ${user.lastName}`}</Text>
          <Text style={{fontSize: 12, color: "#08BEE5"}}>{credential.documentJSON.credentialSubject.data.position}</Text>
        </View>
      </View>

      <View style={styles.propertiesContainer}>
        {properties.map(p => <CredentialProperty label={p.name} valueText={p.value}/>)}
      </View>

      {btnVisible && (
        <View style={styles.propertiesContainer}>
          <CompletableButton
            text={button.text}
            loading={button.loading}
            completed={button.completed}
            onPress={() => {
              button.onPress()
            }}
          />
        </View>
      )}
    </View>
  );

};

const styles = StyleSheet.create({
  container: {
    marginVertical: 16,
    marginHorizontal: 16,
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
  },

  title: {
    marginHorizontal: 16,
    marginVertical: 4,
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },

  userContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    flexDirection: "row",
    alignItems: "center",
  },

  propertiesContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    paddingHorizontal: 16,
    paddingVertical: 16,
  },

});

export default EmploymentCredential;
