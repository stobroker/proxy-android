import React from 'react'
import {Modal, View} from "react-native";
import QRCode from "react-native-qrcode-svg";

const QRViewModel = ({visible, onRequestClose, value}) => {

  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={visible}
      onRequestClose={() => {
        onRequestClose()
      }}
    >
      <View style={{flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center"}}>
        <QRCode value={value} size={380}/>
      </View>
    </Modal>
  )
};

export default QRViewModel;
