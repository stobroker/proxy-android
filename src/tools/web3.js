const Web3 = require('web3');

const network = "https://rinkeby.infura.io/v3/b847c36ac92c4848bb4b78c91e9a4c30"
export const web3 = new Web3(
  new Web3.providers.HttpProvider(network)
);
