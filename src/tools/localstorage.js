import React from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

const AFFINITI_ACCOUNT = "affinity_account_key";
const API_ACCESS_TOKEN = "api_access_token_key";
const ETH_ADDRESS = "eth_address_key";
const PROFILE = "server_profile_key";
const ROLE = "api_role_key";

const Localstorage = {

  setAffinityAccount: async function(account) {
    await AsyncStorage.setItem(AFFINITI_ACCOUNT, JSON.stringify(account))
  },

  getAffinityAccount: async function() {
    const str = await AsyncStorage.getItem(AFFINITI_ACCOUNT)
    return (str != null && str.length > 0) ? JSON.parse(str) : null
  },

  setAccessToken: async function (token) {
    await AsyncStorage.setItem(API_ACCESS_TOKEN, token)
  },

  getAccessToken: async function() {
    return await AsyncStorage.getItem(API_ACCESS_TOKEN)
  },

  setRole: async function (token) {
    await AsyncStorage.setItem(ROLE, token)
  },

  getRole: async function() {
    return await AsyncStorage.getItem(ROLE)
  },

  setEthAddress: async function (address) {
    await AsyncStorage.setItem(ETH_ADDRESS, address)
  },

  getEthAddress: async function() {
    return await AsyncStorage.getItem(ETH_ADDRESS)
  },

  setProfile: async function (json) {
    await AsyncStorage.setItem(PROFILE, JSON.stringify(json))
  },

  getProfile: async function() {
    const str = await AsyncStorage.getItem(PROFILE)
    return str != null ? JSON.parse(str) : null
  },

};

export default Localstorage;
