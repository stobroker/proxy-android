import * as Keychain from "react-native-keychain";

const options = {
  accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
  securityLevel: Keychain.SECURITY_LEVEL.SECURE_HARDWARE,
  storage: Keychain.STORAGE_TYPE.RSA,
  accessible: Keychain.ACCESSIBLE.WHEN_UNLOCKED_THIS_DEVICE_ONLY,
  authenticationType: Keychain.AUTHENTICATION_TYPE.DEVICE_PASSCODE_OR_BIOMETRICS,
  rules: Keychain.SECURITY_RULES.NONE,
};

export async function saveWallet({ address, privateKey }) {
  try {
    await reset();
    await Keychain.setGenericPassword(
      address,
      privateKey,
      {
        accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
        securityLevel: Keychain.SECURITY_LEVEL.SECURE_HARDWARE,
        storage: Keychain.STORAGE_TYPE.RSA,
        accessible: Keychain.ACCESSIBLE.WHEN_PASSCODE_SET_THIS_DEVICE_ONLY,
        authenticationType: Keychain.AUTHENTICATION_TYPE.DEVICE_PASSCODE_OR_BIOMETRICS,
        rules: Keychain.SECURITY_RULES.NONE,
      },
    );
  } catch (err) {
    throw err
  }
}

export async function loadWallet() {
  try {
    const options = {
      authenticationPrompt: {
        title: "Authentication needed",
        subtitle: "to access your Ethereum wallet",
        description: "",
        cancel: "Cancel",
      },
      accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
      securityLevel: Keychain.SECURITY_LEVEL.SECURE_HARDWARE,
      storage: Keychain.STORAGE_TYPE.RSA,
      accessible: Keychain.ACCESSIBLE.WHEN_UNLOCKED_THIS_DEVICE_ONLY,
      authenticationType: Keychain.AUTHENTICATION_TYPE.DEVICE_PASSCODE_OR_BIOMETRICS,
      rules: Keychain.SECURITY_RULES.NONE,
    };
    const credentials = await Keychain.getGenericPassword(options);
    if (credentials) {
      return {
        address: credentials.username,
        privateKey: credentials.password,
      };
    } else {
      throw ("No credentials stored.");
    }
  } catch (err) {
    throw "Could not load credentials. " + err
  }
}

export async function reset() {
  try {
    await Keychain.resetGenericPassword();
  } catch (err) {
    console.error("Could not reset credentials, " + err);
  }
}
