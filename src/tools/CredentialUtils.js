import { EmploymentCredentialType, ETHWalletCredentialType, IDCredentialType } from "../affinity/Affinity";

const PURCHASE_CONTRACT_PROXY_REQUIRED_CREDENTIALS = [
  IDCredentialType,
  EmploymentCredentialType
]

export const CredentialUtils = {

  isCredentialRequired: function(cred, proposalType) {
    if (proposalType === "PURCHASE_CONTRACT_PROXY") {
      for (const type of cred.type) {
        if (PURCHASE_CONTRACT_PROXY_REQUIRED_CREDENTIALS.includes(type))
          return true
      }
    }

    return false;
  },

  getCredentialDisplayName: function(cred) {
    if (cred.type.includes(ETHWalletCredentialType)) {
      return  "ETH Wallet"
    } else if (cred.type.includes(IDCredentialType)) {
      return "ID Credential"
    }else if (cred.type.includes(EmploymentCredentialType)) {
      return"Employment Credential"
    }else if (cred.type.includes("EmailCredentialPersonV1")) {
      return "Email Credential"
    } else {
      if (cred.type.length > 1) return cred.type[1]
      else if (cred.type.length === 1) return cred.type[0]
      else return "Verifiable Credential"
    }
  }
}
