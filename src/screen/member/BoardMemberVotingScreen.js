import React, {useEffect, useState} from "react";
import {RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import {Appbar, Button} from "react-native-paper";
import Countdown from "../../component/Countdown";
import Icon from "react-native-vector-icons/MaterialIcons";
import VotesBlock from "../../component/VotesBlock";
import CredentialProperty from "../../component/CredentialProperty";
import AttachedFile from "../../component/AttachedFile";
import VoteParam from "../../component/VoteParam";
import {IconVotingPassed} from "../../component/Icons";
import ApiService, {BASE_URL} from "../../api/ApiService";
import {Voice} from "../../models/Voice";
import {VotingStatus} from "../../models/VotingStatus";
import dateFormat from 'dateformat';
import Localstorage from "../../tools/localstorage";
import {loadWallet} from "../../tools/WalletSecureStorage";
import {web3} from "../../tools/web3";
import {DocumentDirectoryPath, downloadFile} from "react-native-fs";
import RnHash, {CONSTANTS} from "react-native-hash";
import {EmploymentCredentialType} from "../../affinity/Affinity";

const BoardMemberVotingScreen = ({navigation, route}) => {

  // console.log(JSON.stringify(proposal, null, 2))
  const proxyFileName = route.params.proposal.authorizationDocument.documentURL.split('/').pop()
  const contractFileName = route.params.proposal.additionalDocument.documentURL.split('/').pop()
  const [proposal, setProposal] = useState(route.params.proposal)
  const [submitButtonCfg, setSubmitButtonCfg] = useState({visible: false, onPress: () => {}, text: ""});
  const [submitLoading, setSubmitLoading] = useState(false)
  const [isVoted, setVoted] = useState(false)
  const [voting, setVoting] = useState(null)
  const [votingObject, setVotingObject] = useState({
    participants: 0,
    yes: 0,
    no: 0,
    abstain: 0
  })
  const [refreshing, setRefreshing] = useState(false)

  const [signatureBoardMembers, setSignatureBoardMembers] = useState(proposal.signatureBoardMembers)


  useEffect(async () => {
    reloadVoting()
  }, [])

  useEffect(async () => {
    setSubmitButtonCfg(await getSubmitButtonCfg())
    const ss = await ApiService.getCredentials()
    // console.log(JSON.stringify(ss, null, 2))
  }, [signatureBoardMembers, voting])


  async function reloadVoting() {
    try {
      const voting = await ApiService.getVoting(proposal.issue.id)
      console.log(voting)
      setVotingObject({
        participants: voting.countPossibleVoice,
        yes: voting.countVoiceYes,
        no: voting.countVoiceNo,
        abstain: voting.countVoiceAbstain,
      })
      setVoting(voting)
      setVoted(voting.voted === true)
    } catch (e) {
      alert(JSON.stringify(e))
      console.error(JSON.stringify(e))
    }
  }

  async function reloadProposal() {
    try {
      const targetId = parseInt(route.params.proposal.id)
      const result = await ApiService.getProposalById(targetId)
      setProposal(result)
    } catch (e) {
      console.error(JSON.stringify(e))
      alert("Reload proposals list failed")
    }
  }

  async function sendVoice(voice) {
    try {
      await ApiService.vote(proposal.issue.id, voice)
      await reloadVoting()
    } catch (e) {
      console.error(JSON.stringify(e))
      alert(JSON.stringify(e))
    }
  }

  async function onRefresh() {
    try {
      await reloadVoting()
      await reloadProposal()
    } catch (e) {
      console.error(JSON.stringify(e))
      alert("Refresh failed")
    }
  }

  async function createProxy() {
    console.log("createProxy()")
    try {
      setSubmitLoading(true)
      await ApiService.createProxy(proposal.issue.id, proposal.id)
      setSubmitLoading(false)
      navigation.navigate("Home")
    } catch (e) {
      console.error(JSON.stringify(e))
      alert(JSON.stringify(e))
      setSubmitLoading(true)
    }
  }

  async function signProxy() {
    console.log("signProxy()")
    try {
      setSubmitLoading(true)
      const data = await loadWallet()
      const emplCred = await ApiService.getCredential(EmploymentCredentialType)
      if (emplCred == null) {
        alert("Employment credential not found")
        return
      }
      const account = web3.eth.accounts.privateKeyToAccount(data.privateKey);
      const url = BASE_URL + "/" + proposal.authorizationDocument.documentURL;
      const parts = url.split('/');
      const fileName = parts[parts.length - 1];
      const path = `${DocumentDirectoryPath}/${fileName}`;
      const options = {
        fromUrl: url,
        toFile: path
      }
      const response = await downloadFile(options);

      await response.promise;
      const hash = await RnHash.hashFile(path, CONSTANTS.HashAlgorithms.md5)
      const signResult = account.sign(hash)

      await ApiService.boardMemberSignProxy(proposal.id, signResult.signature);
      let tmp = JSON.parse(JSON.stringify(signatureBoardMembers))
      tmp.push({ethereumAddress: account.address, signature: signResult.signature})
      setSignatureBoardMembers(tmp)


      console.log("Add cred to proposal")
      const provideResult = await ApiService.boardMemberProvideVCtoProposal(proposal.id, [emplCred.id])
      console.log("provideResult = ", provideResult)

      setSubmitLoading(false)

    } catch (e) {
      setSubmitLoading(true)
      console.error("sign proxy:", JSON.stringify(e))
    }
  }

  const voteButtons = () => {
    return (
      <View style={{flexDirection: "row", marginTop: 24}}>

        <Button
          mode={"contained"}
          uppercase={false}
          color={"#2CC68F"}
          style={styles.voteButton}
          icon={({size, color}) => (
            <Icon name="done" color={"white"} size={16}/>
          )}
          labelStyle={{color: "white"}}
          onPress={() => sendVoice(Voice.YES)}
        >Yes</Button>

        <Button
          mode={"contained"}
          uppercase={false}
          color={"#F5A623"}
          style={styles.voteButton}
          labelStyle={{color: "white"}}
          onPress={() => sendVoice(Voice.ABSTAIN)}

        >Abstain</Button>

        <Button
          mode={"contained"}
          uppercase={false}
          color={"#FF6969"}
          style={styles.voteButton}
          icon={({size, color}) => (
            <Icon name="close" color={"white"} size={16}/>
          )}
          labelStyle={{color: "white"}}
          onPress={() => sendVoice(Voice.NO)}
        >No</Button>

      </View>
    )
  }

  async function getSubmitButtonCfg() {
    const address = await Localstorage.getEthAddress()
    const createProxyAvailable= voting != null && signatureBoardMembers.length === parseInt(voting.countPossibleVoice)
    const isSigned = signatureBoardMembers.filter(i=> i.ethereumAddress === address).length > 0

    if (voting?.status === VotingStatus.COMPLETED) {
      if (isSigned) {
        if (createProxyAvailable) {
          console.log("create proxy")
          return {
            visible: true,
            onPress: () => {createProxy()},
            text: "Create Proxy"
          }
        } else {
          console.log("not available")
          return {
            visible: false,
            onPress: () => {},
            text: ""
          }
        }
      } else {
        console.log("sign")
        return {
          visible: true,
          onPress: () => {signProxy()},
          text: "Sign"
        }
      }
    } else {
      console.log("not completed")
      return {
        visible: false,
        onPress: () => {},
        text: ""
      }
    }
  }


  function getTotalProgress() {
    return Math.round((votingObject.yes + votingObject.no + votingObject.abstain) / votingObject.participants * 100)
  }

  console.log(submitButtonCfg)

  return (
    <SafeAreaView>

      <Appbar
        dark={false}
        style={{backgroundColor: "#FFFFFF"}}
      >
        <Appbar.BackAction onPress={() => {
          navigation.navigate("Home")
        }}/>
        <Appbar.Content title="Voting"/>
      </Appbar>

      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>

        <View style={{marginBottom: 56}}>
          <View style={styles.container}>

            <View style={styles.detailsContainer}>

              <View style={{flexDirection: "row"}}>
                <View style={{flex: 10}}>
                  <Text style={styles.title}>{proposal.issue.title}</Text>
                </View>
                {voting?.status === VotingStatus.COMPLETED && (
                  <View style={{flex: 2, flexDirection: "row", alignItems: "center"}}>
                    <IconVotingPassed color={"#2CC68F"}/>
                    <Text style={{color: "#2CC68F"}}>Passed</Text>
                  </View>
                )}
              </View>

              {
                (voting?.status === VotingStatus.LAUNCHED) ? (
                  <Countdown endDate={voting.endDate} style={{marginVertical: 12}}/>
                ) : (
                  <View style={{
                    flexDirection: "row",
                    marginVertical: 12,
                    justifyContent: "flex-start",
                    alignItems: "center"
                  }}>
                    {(
                      <>
                        <Icon name={"schedule"} size={16} color={"#8FA4B5"} style={{marginEnd: 12}}/>
                        {voting?.endDate != null &&
                        <Text style={{color: "#637381"}}>{dateFormat(new Date(voting.endDate), "yyyy-dd-mm")}</Text>}
                      </>
                    )}
                  </View>
                )
              }

              <Text>{proposal.issue.description}</Text>
            </View>

            <View style={styles.votesContainer}>

              <VotesBlock
                titleStyle={styles.containerTitle} votingObject={votingObject}
              />

              {!(isVoted || voting?.status !== VotingStatus.LAUNCHED) && voteButtons()}
            </View>
          </View>


          <VoteParam title={"Support"}
                     progress={getTotalProgress()}
                     need={voting != null ? voting.supportPercentNeed : 0}
          />


          <VoteParam title={"Minimum approval"}
                     progress={getTotalProgress()}
                     need={voting != null ? voting.minimalApprovalPercentNeed : 0}

          />


          <View style={styles.container}>
            <Text style={[styles.containerTitle, {marginHorizontal: 24, marginVertical: 8}]}>Proposal Details</Text>
            <View style={[styles.topBorderContainer, {flexDirection: "row", alignItems: "center"}]}>

              <View style={{flex: 9}}>
                <Text style={{fontSize: 18, color: "#000000"}}>Purchase Contract Proxy</Text>
              </View>
              <View style={{flex: 3}}>
                <Button
                  mode={"contained"}
                  uppercase={false}
                  color={"white"}
                  labelStyle={{color: "#000000", fontSize: 12, letterSpacing: 0.2, marginHorizontal: 0}}
                  onPress={() => {
                    navigation.navigate("ProposalDetails", {data: proposal})
                  }}
                >Details</Button>
              </View>

            </View>

            <View style={styles.topBorderContainer}>
              <CredentialProperty label={"Authorization Document"}
                                  valueComponent={<AttachedFile name={proxyFileName}/>}/>
              <CredentialProperty label={"Additional Document"}
                                  valueComponent={<AttachedFile name={contractFileName}/>}/>
            </View>
          </View>

          {
            submitButtonCfg.visible && (
              <View style={{marginHorizontal: 16, marginVertical: 24}}>
                <Button
                  mode={"contained"}
                  uppercase={false}
                  color={"#08BEE5"}
                  labelStyle={{color: "#FFFFFF", fontSize: 14, letterSpacing: 0.2}}
                  onPress={submitButtonCfg.onPress}
                >{submitButtonCfg.text}</Button>
              </View>
            )
          }

        </View>
      </ScrollView>

    </SafeAreaView>
  );

};

const styles = StyleSheet.create({

  container: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 8,
  },

  title: {
    fontSize: 20,
    color: "#000000",
  },

  description: {
    fontSize: 14,
    color: "#212B36",
  },

  detailsContainer: {
    padding: 16,
  },

  votesContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },

  containerTitle: {
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },

  voteButton: {
    flex: 1,
    marginHorizontal: 4,
  },

  topBorderContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },

});

export default BoardMemberVotingScreen;
