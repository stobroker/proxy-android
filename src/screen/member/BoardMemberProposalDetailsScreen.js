import React, {useEffect, useState} from "react";
import {RefreshControl, ScrollView, StyleSheet, Text, View} from "react-native";
import {Appbar, Button} from "react-native-paper";
import IssueStatus from "../../component/IssueStatus";
import PersonPreviewSmall from "../../component/PersonPreviewSmall";
import dateFormat from "dateformat";
import AttachedFile from "../../component/AttachedFile";
import HashSum from "../../component/HashSum";
import CompletableButton from "../../component/CompletableButton";
import IssueCredential from "../../component/IssueCredential";
import CredentialProperty from "../../component/CredentialProperty";
import * as ProposalType from "../../models/ProposalType"
import {AffinityHelper, EmploymentCredentialType, IDCredentialType} from "../../affinity/Affinity";
import QRScannerModal from "../../component/QRScannerModal";
import QRViewModal from "../../component/QRViewModal";
import {DocumentDirectoryPath, downloadFile} from 'react-native-fs';
import ApiService, {BASE_URL} from "../../api/ApiService";
import RnHash, {CONSTANTS} from "react-native-hash";
import {web3} from "../../tools/web3";

const BoardMemberProposalDetailsScreen = ({navigation, route}) => {

  const proposal = route.params.data;
  const proxyFileName = proposal.authorizationDocument.documentURL.split('/').pop()
  const contractFileName = proposal.additionalDocument.documentURL.split('/').pop()
  console.log(proposal)

  useEffect(async () => {
    await onRefresh()
  }, [])

  const [refreshing, setRefreshing] = useState(false)
  const [proposalVerificationLoading, setProposalVerificationLoading] = useState(false)
  const [proposalVerified, setProposalVerified] = useState(false)
  const [credentials, setCredentials] = useState(proposal.verifiedCredentialDocuments)

  const [shareVcRequests, setShareVcRequests] = useState([])
  const [verifiedCredentials, setVerifiedCredentials] = useState([])

  async function verifyDoc(url, signature) {
    const parts = url.split('/');
    const fileName = parts[parts.length - 1];
    const path = `${DocumentDirectoryPath}/${fileName}`;
    const options = {
      fromUrl: url,
      toFile: path
    }
    const response = await downloadFile(options);

    await response.promise;
    const hash = await RnHash.hashFile(path, CONSTANTS.HashAlgorithms.md5)
    console.log("hash", hash);
    const recoverResult = web3.eth.accounts.recover(hash, signature)
    console.log("recoverResult", recoverResult, proposal.manager)
    return recoverResult === proposal.manager.ethereumAddress
  }

  async function verifyFiles() {
    const authDocVerified = await  verifyDoc(`${BASE_URL}/${proposal.authorizationDocument.documentURL}`, proposal.authorizationDocument.authDocSign)
    const additionalDocVerified = await  verifyDoc(`${BASE_URL}/${proposal.additionalDocument.documentURL}`, proposal.additionalDocument.additionalDocSign)
    setProposalVerified(authDocVerified && additionalDocVerified)
  }

  function getShareVcReq(credentialId) {
    for (let req of shareVcRequests) {
      if (parseInt(req.credentialId) === parseInt(credentialId)) {
        return req
      }
    }
    return null
  }

  async function onRequestShareVcClick(cred) {
    try {
      const reqToken = await AffinityHelper.getShareCredentialsRequest([{type: cred.documentJSON.type}])
      await ApiService.boardMemberReqShareVC(proposal.id, cred.id, reqToken, null)
      await onRefresh()
    } catch (e) {
      console.error(JSON.stringify(e))
      alert(e)
    }
  }

  async function onVerifyVcClick(shareReq, cred) {
    try {
      const verified = await AffinityHelper.verifyCredential(shareReq.responseToken)
      if (verified) {
        await ApiService.markCredAsVerified(proposal.id, cred.id)
        let tmp = JSON.parse(JSON.stringify(verifiedCredentials))
        tmp.push(parseInt(cred.id))
        setVerifiedCredentials(tmp)
      } else {
        alert("Credential is wrong")
      }
    } catch (e) {
      console.error(JSON.stringify(e))
      if (e.hasOwnProperty("_code") && e._code === "COR-19") {
        alert("Token expired")
        try {
          await ApiService.deleteShareVCReq(shareReq.id)
          await onRefresh()
        } catch (e) {
          console.error(JSON.stringify(e))
          alert(JSON.stringify(e))
        }
      } else {
        alert(e)
      }

    }
  }

  function getCredentialBtn(cred) {
    const shareReq = getShareVcReq(cred.id);

    let btnText, btnCompleted, onPress;
    if (shareReq == null) {
      btnText = "Request"
      btnCompleted = false
      onPress = () => {
        onRequestShareVcClick(cred)
      }
    } else {
      if (shareReq.responseToken == null) {
        btnText = "Requested"
        btnCompleted = true
      } else {
        if (verifiedCredentials.includes(parseInt(cred.id))) {
          btnText = "Verified"
          btnCompleted = true
        } else {
          btnText = "Verify"
          btnCompleted = false
          onPress = () => {
            onVerifyVcClick(shareReq, cred)
          }
        }
      }
    }

    return {
      text: btnText,
      loading: false,
      onPress: onPress,
      completed: btnCompleted
    }
  }

  function renderCredentials(credentials) {
    let employmentProof, managerId = null;
    for (let cred of credentials) {
      if (cred.documentJSON.type.includes(EmploymentCredentialType)) {
        console.log(cred.id)
        const properties = [
          {name: "Employer", value: cred.documentJSON.credentialSubject.data.employer},
          {name: "Position", value: cred.documentJSON.credentialSubject.data.position},
          // {name: "Start date", value: cred.documentJSON.credentialSubject.data.startDate},
          // {name: "Start date", value: dateFormat(new Date(cred.documentJSON.credentialSubject.data.startDate), "yyyy-mm-dd")}
        ]


        employmentProof = (<IssueCredential name={"Employment Proof"}
                                            user={cred.user}
                                            properties={properties}
                                            button={getCredentialBtn(cred)}
        />)
      } else if (cred.documentJSON.type.includes(IDCredentialType)) {
        const properties = [
          {
            name: "Date of birth",
            value: dateFormat(new Date(cred.documentJSON.credentialSubject.data.dateOfBirth), "yyyy-mm-dd")
          },
          {name: "Citizenship", value: cred.documentJSON.credentialSubject.data.citizenship},
          {name: "Document Number", value: cred.documentJSON.credentialSubject.data.documentNumber},
          {name: "Personal code", value: cred.documentJSON.credentialSubject.data.personalDate},
          {
            name: "Date of expiry",
            value: dateFormat(new Date(cred.documentJSON.credentialSubject.data.expirationDate), "yyyy-mm-dd")
          },
        ]
        managerId = (<IssueCredential name={"Manager ID"}
                                      user={cred.user}
                                      properties={properties}
                                      button={getCredentialBtn(cred)}
        />)
      }
    }

    return (
      <>
        {employmentProof}
        {managerId}
      </>
    )
  }

  async function onRefresh() {
    try {
      setShareVcRequests(await ApiService.getBoardMemberShareVcReq(proposal.id))
      setRefreshing(false)
    } catch (e) {
      setRefreshing(false)
      alert(e)
    }
  }

  return (
    <View>
      <Appbar
        dark={false}
        style={{backgroundColor: "#FFFFFF"}}
      >
        <Appbar.BackAction onPress={() => {
          navigation.goBack()
        }}/>
        <Appbar.Content title="Proposal"/>

      </Appbar>

      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>

        <View style={styles.container}>

          <View style={styles.block}>

            <View style={styles.subBlock}>
              <View style={{flexDirection: "row"}}>
                <Text style={styles.title}>{ProposalType.getDisplayName(proposal.type)}</Text>
                <IssueStatus status={proposal.status}/>
              </View>
              <Text style={styles.description}>{proposal.description}</Text>
            </View>

            <View style={styles.propertiesBlock}>
              <CredentialProperty label={"ID"} valueText={proposal.id}/>
              <CredentialProperty label={"Date"} valueText={dateFormat(proposal.createdAt, "yyyy-mm-dd")}/>
              <CredentialProperty label={"Manager"} valueComponent={(
                <PersonPreviewSmall firstName={proposal.manager.firstName} lastName={proposal.manager.lastName}
                                    photo={null}/>)}/>
            </View>

            <View style={styles.propertiesBlock}>
              <CredentialProperty label="Authorization Document" valueComponent={(<AttachedFile name={proxyFileName}/>)}/>
              <CredentialProperty label={"Additional Document"}
                                  valueComponent={(<AttachedFile name={contractFileName}/>)}/>
              <CredentialProperty label={"Authorization Document Signature"}
                                  valueComponent={(<HashSum hash={proposal.authorizationDocument.authDocSign}/>)}/>
              <CredentialProperty label={"Additional Document Signature"}
                                  valueComponent={(<HashSum hash={proposal.additionalDocument.additionalDocSign}/>)}/>
            </View>

            <View style={styles.propertiesBlock}>
              <CompletableButton
                text={"Verify"}
                loading={proposalVerificationLoading}
                completed={proposalVerified}
                onPress={() => verifyFiles()}
              />
            </View>
          </View>

          {renderCredentials(credentials)}

          <View style={{flexDirection: "column", marginVertical: 32}}>
            <View style={{flex: 1}}>
              <Button
                mode={"contained"}
                uppercase={false}
                color={"#08BEE5"}
                style={{elevation: 4, marginVertical: 4, marginHorizontal: 16,}}
                labelStyle={{color: "#FFFFFF"}}
                onPress={() => {
                  navigation.navigate("CreateIssue", {proposal: proposal})
                }}
              >Create</Button>

              <View style={{flex: 1}}>
                <Button
                  mode={"text"}
                  uppercase={false}
                  color={"#FFFFFF"}
                  style={{elevation: 0, marginVertical: 4, marginHorizontal: 16,}}
                  labelStyle={{color: "#FF0000"}}
                  onPress={() => {
                  }}
                >Reject</Button>
              </View>
            </View>

          </View>


        </View>

      </ScrollView>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 56,
    marginTop: 16,
  },

  block: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderRadius: 4,
    borderWidth: 1,
    marginHorizontal: 16,
  },

  subBlock: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },

  propertiesBlock: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
  },

  title: {
    fontSize: 18,
    color: "#000000",
    marginEnd: 16,
  },

  description: {
    fontSize: 12,
    color: "#212B36",
    marginTop: 16,
  },

});

export default BoardMemberProposalDetailsScreen;
