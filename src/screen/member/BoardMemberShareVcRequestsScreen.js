import React, {useEffect, useState} from 'react'
import {RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Button} from "react-native-paper";
import ApiService from "../../api/ApiService";
import {CredentialUtils} from "../../tools/CredentialUtils";
import {AffinityHelper} from "../../affinity/Affinity";
import Icon from "react-native-vector-icons/MaterialIcons";

const BoardMemberShareVcRequestsScreen = ({}) => {

  const [refreshing, setRefreshing] = useState(false)
  const [credentials, setCredentials] = useState([])
  const [requests, setRequests] = useState([])

  useEffect(async () => {
    setCredentials(await ApiService.getCredentials())
    await onRefresh()
  }, [])

  function getCredentialById(id) {
    for (let cred of credentials) {
      if (parseInt(cred.id) === parseInt(id)) return cred
    }

    return null
  }


  function renderEmptyList() {
    return (
      <View style={{flex: 1, justifyContent: "center", alignItems: "center", padding: 32}}>
        <Text>Requests list is empty</Text>
      </View>
    )
  }

  function renderRequests() {

    function renderItem({item, index}) {

      async function onShareClick() {
        try {
          const resToken = await AffinityHelper.getShareCredentialsResponse(item.reqToken)
          await ApiService.boardMemberShareVcResponse(item.id, resToken)
          await onRefresh()
        } catch (e) {
          console.error(JSON.stringify(e))
          alert(JSON.stringify(e))
        }
      }

      return (
        <View key={"req-item-" + item.id}>
          <View style={index === 0 ? styles.firstRequestItem : styles.requestItem}>

            <View style={{flex: 1}}>
              <View style={{backgroundColor: "#32FFF5", width: 32, height: 32, borderRadius: 6}}/>
            </View>

            <View style={{flex: 8, marginStart: 16}}>
              <Text style={{
                color: "#212B36",
                fontSize: 14,
              }}>{item.name}</Text>

              <Text style={{
                color: "#637381",
                fontSize: 12,
              }}>{"Proposal #" + item.proposalId}</Text>
              <Text style={{
                color: "#637381",
                fontSize: 12,
              }}>{"Request ID #" + item.id}</Text>
            </View>

            <View style={{flex: 4, flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
              <TouchableOpacity onPress={async () => {
                await ApiService.deleteShareVCReq(item.id)
                await onRefresh()
              }}>
                <Icon name={"close"} color={"#FF6969"} size={24} style={{
                  height: 32,
                  width: 32,
                  padding: 4,
                  elevation: 4,
                  backgroundColor: "#FFFFFF",
                  borderRadius: 4,
                }}/>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => {
                onShareClick()
              }}>
                <Icon name={"check-circle"} color={"#2CC68F"} size={24} style={{
                  height: 32,
                  width: 32,
                  padding: 4,
                  elevation: 4,
                  backgroundColor: "#FFFFFF",
                  borderRadius: 4,
                  marginStart: 4
                }}/>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      )
    }

    const listItems = [];
    for (let i = 0; i < requests.length; i++) {
      const req = requests[i]
      console.log("obj", req)
      const cred = getCredentialById(req.credentialId)
      const item = {
        id: req.id,
        name: cred != null ? CredentialUtils.getCredentialDisplayName(cred.documentJSON) : "Unknown",
        proposalId: req.proposalId,
        reqToken: req.requestToken
      }
      listItems.push(renderItem({item: item, index: i}))
    }

    return listItems
  }

  async function onRefresh() {
    try {
      const reqList = await ApiService.boardMemberVcRequests();
      setRequests(reqList)
      setRefreshing(false)
    } catch (e) {
      setRefreshing(false)
      alert(e)
    }
  }

  return (
    <ScrollView
      contentContainerStyle={{flex: 1}}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }>

      <Text style={{
        marginHorizontal: 16,
        marginTop: 32,
        marginBottom: 12,
        fontSize: 20
      }}>Share Credential Requests</Text>

      <View style={styles.container}>


        {requests.length > 0 ? renderRequests() : renderEmptyList()}

      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    marginVertical: 16,
    marginHorizontal: 16,
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
  },

  requestItem: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 8,
    marginHorizontal: 16,
    marginVertical: 4,
    borderTopColor: "#DDE4E9",
    borderTopWidth: 1
  },

  firstRequestItem: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 8,
    marginHorizontal: 16,
    marginVertical: 4,
  },


})

export default BoardMemberShareVcRequestsScreen
