import React, {useState} from "react";
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import Slider from '@react-native-community/slider'
import {Appbar, Button, TextInput} from "react-native-paper";
import {TextInputTheme} from "../../themes/AppTheme";
import CustomDateTimePicker from "../../component/CustomDateTimePicker";
import ApiService from "../../api/ApiService";

const BoardMemberCreateIssueScreen = ({navigation, route}) => {

  const proposal = route.params.proposal;
  const [title, setTitle] = useState()
  const [description, setDescription] = useState()
  const [startDate, setStartDate] = useState()
  const [endDate, setEndDate] = useState()
  const [support, setSupport] = useState(50)
  const [minimumApproval, setMinimumApproval] = useState(50)
  const [createLoading, setCreateLoading] = useState(false)


  async function onCreateClick() {
    try {
      setCreateLoading(true)
      const issue = await ApiService.createIssue(
        proposal.id,
        title,
        description,
        startDate,
        endDate,
        support,
        minimumApproval
      )

      const tmp = JSON.parse(JSON.stringify(proposal))
      tmp.issue = issue

      setCreateLoading(false)
      navigation.navigate("VotingDetails", {
        proposal: tmp
      })
    } catch (e) {
      setCreateLoading(false)
      console.error(JSON.stringify(e))
      alert(JSON.stringify(e))
    }
  }

  return (
    <SafeAreaView>

      <Appbar
        dark={false}
        style={{backgroundColor: "#FFFFFF"}}
      >
        <Appbar.BackAction onPress={() => {
          navigation.goBack()
        }}/>
        <Appbar.Content title="Create Issue"/>
      </Appbar>

      <ScrollView>

        <View style={styles.container}>

          <TextInput
            mode={"flat"}
            placeholder={"Title"}
            underlineColor="white"
            style={[styles.textInput, {height: 40}]}
            theme={TextInputTheme}
            onChange={(event) => {
              let text = event.nativeEvent.text
              setTitle(text)
            }}/>

          <TextInput
            mode={"flat"}
            placeholder={"Description"}
            underlineColor="white"
            multiline={true}
            numberOfLines={4}
            style={styles.textInput}
            theme={TextInputTheme}
            onChange={(event) => {
              let text = event.nativeEvent.text
              setDescription(text)
            }}/>

          <CustomDateTimePicker hint={"Start Date"} style={{marginVertical: 8}}
                                onDateChanged={(date) => setStartDate(date)}/>
          <CustomDateTimePicker hint={"End Date"} style={{marginVertical: 8}}
                                onDateChanged={(date) => setEndDate(date)}/>

          <View style={{marginTop: 8}}>
            <View style={{flexDirection: "row"}}>
              <Text style={{color: "#637381", textTransform: "uppercase"}}>Support</Text>
            </View>
            <View style={{flexDirection: "row"}}>
              <Slider
                maximumValue={100}
                minimumValue={0}
                step={1}
                thumbTintColor={"#08BEE5"}
                minimumTrackTintColor={"#08BEE5"}
                maximumTrackTintColor={"#08BEE5"}
                value={support}
                onValueChange={(value) => {
                  setSupport(value)
                }}
                style={{
                  flex: 9,
                }}
              />
              <TextInput
                mode={"flat"}
                placeholder={"0"}
                underlineColor="white"
                style={[styles.textInput, {flex: 3, height: 40}]}
                theme={TextInputTheme}
                value={support + ""}
                right={<TextInput.Affix text={"%"}/>}
                onChange={(event) => {
                  let text = event.nativeEvent.text
                  if (text.length > 0) {
                    setSupport(parseInt(text))
                  }
                }}
              />
            </View>
          </View>

          <View style={{marginTop: 8}}>
            <View style={{flexDirection: "row"}}>
              <Text style={{color: "#637381", textTransform: "uppercase"}}>Minimum approval</Text>
            </View>
            <View style={{flexDirection: "row"}}>
              <Slider
                maximumValue={100}
                minimumValue={0}
                step={1}
                thumbTintColor={"#08BEE5"}
                minimumTrackTintColor={"#08BEE5"}
                maximumTrackTintColor={"#08BEE5"}
                onValueChange={(value) => {
                  setMinimumApproval(value)
                }}
                value={minimumApproval}
                style={{
                  flex: 9,
                }}
              />

              <TextInput
                mode={"flat"}
                placeholder={"0"}
                underlineColor="white"
                keyboardType='numeric'
                style={[styles.textInput, {flex: 3, height: 40}]}
                theme={TextInputTheme}
                value={minimumApproval + ""}
                right={<TextInput.Affix text={"%"}/>}
                onChange={(event) => {
                  let text = event.nativeEvent.text
                  if (text.length > 0) {
                    setMinimumApproval(parseInt(text))
                  }
                }}
              />

            </View>
          </View>

        </View>

        <Button
          mode={"contained"}
          style={{marginVertical: 32, marginHorizontal: 16}}
          color={"#08BEE5"}
          labelStyle={{color: "#FFFFFF"}}
          loading={createLoading}
          disabled={createLoading}
          uppercase={false}
          onPress={() => {
            onCreateClick()
          }}>Create</Button>


      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    margin: 16,
    padding: 16
  },

  inputsContainer: {
    margin: 12
  },

  textInput: {
    flex: 1,
    marginVertical: 8,
    textAlignVertical: "top",
    borderRadius: 4,
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDE4E9",
  },
});

export default BoardMemberCreateIssueScreen;
