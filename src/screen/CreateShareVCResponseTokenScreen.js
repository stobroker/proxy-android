import React, {useState} from 'react'
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import {View} from "react-native";
import QRCode from "react-native-qrcode-svg";
import {Button} from "react-native-paper";
import {AffinityHelper} from "../affinity/Affinity";

const CreateShareVCResponseTokenScreen = ({navigation}) => {
  const [scanResult, setScanResult] = useState()

  return (
    <View style={{flex: 1}}>
      <View style={{flexDirection: "row", flex: 1, justifyContent: "center", alignItems: "center"}}>
        {scanResult == null
        ? (
            <QRCodeScanner
              onRead={async (e) => {
                const token = await AffinityHelper.getShareCredentialsResponse(e.data)
                // setScanResult(token)
              }}
              flashMode={RNCamera.Constants.FlashMode.off}
            />
          )
        : (<QRCode value={scanResult} size={380}/>)
        }
      </View>
      <View style={{justifyContent: "flex-end", alignItems: "center"}}>
        {scanResult != null && (<Button onPress={() => setScanResult(null)}>Retry</Button>)}
      </View>

    </View>
  )
}

export default CreateShareVCResponseTokenScreen;
