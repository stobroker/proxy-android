import React from 'react';
import { Image, Text, View } from "react-native";


const SplashScreen = ({navigation}) => {
  return (
    <View style={{
      flex: 1,
      backgroundColor: "#2CB6E1",
      justifyContent: "center",
      alignItems: "center",
    }}/>
  )
};

export default SplashScreen;
