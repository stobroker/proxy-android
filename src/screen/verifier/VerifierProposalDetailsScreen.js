import React, {useEffect, useState} from 'react'
import {Image, RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import {AffinityHelper, EmploymentCredentialType, IDCredentialType} from "../../affinity/Affinity";
import EmploymentCredential from "../../component/EmploymentCredential";
import IdCredential from "../../component/IdCredential";
import {Appbar, Button} from "react-native-paper";
import dateFormat from "dateformat";
import AttachedFile from "../../component/AttachedFile";
import CompletableButton from "../../component/CompletableButton";
import CredentialProperty from "../../component/CredentialProperty";
import PersonPreviewSmall from "../../component/PersonPreviewSmall";
import HashSum from "../../component/HashSum";
import ApiService, {BASE_URL} from "../../api/ApiService";
import {DocumentDirectoryPath, downloadFile} from "react-native-fs";
import RnHash, {CONSTANTS} from "react-native-hash";
import {web3} from "../../tools/web3";

const VerifierProposalDetailsScreen = ({navigation, route}) => {

  const proposal = route.params.proposal
  const credentials = proposal.verifiedCredentialDocuments;
  const proxyFileName = proposal.authorizationDocument.documentURL.split('/').pop()
  const contractFileName = proposal.additionalDocument.documentURL.split('/').pop()

  let valitTillDate = new Date(proposal.issue.createdAt);
  switch (proposal.validity) {
    case  "1 month": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+1));
      break;
    }
    case  "6 month": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+6));
    }
    case  "1 year": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+12));
    }
  }

  // console.log(JSON.stringify(proposal, null, 2))

  const [refreshing, setRefreshing] = useState(false)
  const [contractVerified, setContractVerified] = useState(false)
  const [contractVerifyLoading, setContractVerifyLoading] = useState(false)
  const [shareVcRequests, setShareVcRequests] = useState([])
  const [verifiedCredentials, setVerifiedCredentials] = useState([])
  const [proxyVerified, setProxyVerified] = useState(false)
  const [proxyVerifyLoading, setProxyVerifyLoading] = useState(false)

  useEffect(async () => {
    await onRefresh()
  }, [])

  function renderCredentials(credentials) {
     let result = []
      for (let cred of credentials) {

        if (cred.documentJSON.type.includes(EmploymentCredentialType)) {
          result.push(<EmploymentCredential btnVisible={true} credential={cred} button={getCredentialBtn(cred)}/>)
        } else if (cred.documentJSON.type.includes(IDCredentialType)) {
          result.push(<IdCredential credential={cred} btnVisible={true} button={getCredentialBtn(cred)}/>)
        }
      }

      return result
  }

  async function onVerifyProxyClick() {
    try {
      setProxyVerifyLoading(true)
      const url = BASE_URL + "/" + proposal.authorizationDocument.documentURL;
      const parts = url.split('/');
      const fileName = parts[parts.length - 1];
      const path = `${DocumentDirectoryPath}/${fileName}`;
      const options = {
        fromUrl: url,
        toFile: path
      }
      const response = await downloadFile(options);

      await response.promise;
      const hash = await RnHash.hashFile(path, CONSTANTS.HashAlgorithms.md5)
      console.log("hash", hash);

      for (let memberSignature of proposal.signatureBoardMembers) {
        const recoverResult = web3.eth.accounts.recover(hash, memberSignature.signature)
        console.log("recoverResult", recoverResult, proposal.manager)
        if (recoverResult !== memberSignature.ethereumAddress) {
          throw "Signature is not valid"
        }
      }
      setProxyVerified(true)
      setProxyVerifyLoading(false)

    } catch (e) {
      setProxyVerifyLoading(false)

      alert("Proxy is not valid: " + e)
    }
  }

  async function onVerifyContractClick() {
    try {
      setContractVerifyLoading(true)
      const url = BASE_URL + "/" + proposal.additionalDocument.documentURL;
      const parts = url.split('/');
      const fileName = parts[parts.length - 1];
      const path = `${DocumentDirectoryPath}/${fileName}`;
      const options = {
        fromUrl: url,
        toFile: path
      }
      const response = await downloadFile(options);

      await response.promise;
      const hash = await RnHash.hashFile(path, CONSTANTS.HashAlgorithms.md5)
      console.log("hash", hash);
      const recoverResult = web3.eth.accounts.recover(hash, proposal.additionalDocument.additionalDocSign)
      if (recoverResult === proposal.manager.ethereumAddress) {
        setContractVerified(true)
      } else {
        throw "Wrong signature"
      }
      setContractVerifyLoading(false)
    } catch (e) {
      alert("Contract is not valid:" + e)
      setContractVerifyLoading(false)
    }
  }

  async function onVerifyVcClick(shareReq, cred) {
    try {
      const verified = await AffinityHelper.verifyCredential(shareReq.responseToken)
      if (verified) {
        // await ApiService.markCredAsVerified(proposal.id, cred.id)
        let tmp = JSON.parse(JSON.stringify(verifiedCredentials))
        tmp.push(parseInt(cred.id))
        setVerifiedCredentials(tmp)
      } else {
        alert("Credential is wrong")
      }
    } catch (e) {
      console.error(JSON.stringify(e))
      if (e.hasOwnProperty("_code") && e._code === "COR-19") {
        alert("Token expired")
        try {
          await ApiService.deleteShareVCReq(shareReq.id)
          await onRefresh()
        } catch (e) {
          console.error(JSON.stringify(e))
          alert(JSON.stringify(e))
        }
      } else {
        alert(e)
      }

    }
  }

  async function onRefresh() {
    try {
      setShareVcRequests(await ApiService.verifierGetProposalShareVcRequests(proposal.id))
      setRefreshing(false)
    } catch (e) {
      setRefreshing(false)
      alert(e)
    }
  }

  function renderMembersSign() {
    let tmp = [];
    const signs = proposal.signatureBoardMembers;
    for (let i = 0; i < signs.length; i++) {
      let signature = signs[i];
      tmp.push(  <CredentialProperty label={`${signature.firstName} ${signature.lastName} Address`}
                                     valueComponent={(<HashSum hash={signature.ethereumAddress}/>)}/>)
      tmp.push(  <CredentialProperty label={`${signature.firstName} ${signature.lastName} Signature`}
                                     valueComponent={(<HashSum hash={signature.signature}/>)}/>)
    }
    return tmp
  }

  function getShareVcReq(credentialId) {
    for (let req of shareVcRequests) {
      if (parseInt(req.credentialId) === parseInt(credentialId)) {
        return req
      }
    }
    return null
  }

  async function onRequestShareVcClick(cred) {
    try {
      const reqToken = await AffinityHelper.getShareCredentialsRequest([{type: cred.documentJSON.type}])
      await ApiService.verifierCreateShareVcRequest(proposal.id, cred.id, reqToken, null)
      await onRefresh()
    } catch (e) {
      console.error(JSON.stringify(e))
      alert(e)
    }
  }

  function getCredentialBtn(cred) {
    const shareReq = getShareVcReq(cred.id);

    let btnText, btnCompleted, onPress;
    if (shareReq == null) {
      btnText = "Request"
      btnCompleted = false
      onPress = () => {
        onRequestShareVcClick(cred)
      }
    } else {
      if (shareReq.responseToken == null) {
        btnText = "Requested"
        btnCompleted = true
      } else {
        if (verifiedCredentials.includes(parseInt(cred.id))) {
          btnText = "Verified"
          btnCompleted = true
        } else {
          btnText = "Verify"
          btnCompleted = false
          onPress = () => {
            onVerifyVcClick(shareReq, cred)
          }
        }
      }
    }

    return {
      text: btnText,
      loading: false,
      onPress: onPress,
      completed: btnCompleted
    }
  }

  return (
    <SafeAreaView>

      <Appbar
        dark={false}
        style={{backgroundColor: "#FFFFFF"}}
      >
        <Appbar.BackAction onPress={() => {
          navigation.goBack()
        }}/>
        <Appbar.Content title="Proposal"/>
      </Appbar>

      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>

        <View style={{paddingBottom: 56}}>

          <View style={styles.container}>
            <Text style={[styles.containerTitle, {marginHorizontal: 24, marginVertical: 8}]}>Proxy</Text>

            <View style={[styles.topBorderContainer, {flexDirection: "row", alignItems: "center"}]}>
              <View style={{flex: 2}}>
                <Image source={require("../../../assets/img/company_logo.png")} style={{width: 64, height: 64}}/>
              </View>
              <View style={{flex: 10}}>
                <Text style={{fontSize: 22, marginStart: 16}}>Acme Corp</Text>
              </View>
            </View>

            <View style={styles.topBorderContainer}>
              <CredentialProperty label="Authorization Document" valueComponent={(<AttachedFile name={proxyFileName}/>)}/>
              <CredentialProperty label="Issue date"
                                  valueText={dateFormat(new Date(proposal.createdAt), "yyyy-dd-mm")}/>
              <CredentialProperty label="Valit till"
                                  valueText={dateFormat(valitTillDate, "yyyy-dd-mm")}/>
              <CredentialProperty label="Issued to"
                                  valueText={"Hello"}
                                  valueComponent={(
                                    <PersonPreviewSmall
                                      firstName={proposal.manager?.firstName}
                                      lastName={proposal.manager?.lastName}
                                      photo={null}/>)}
              />
            </View>
            <View style={styles.topBorderContainer}>
              <Text style={[styles.containerTitle]}>Signature Information</Text>
            </View>
            <View style={styles.topBorderContainer}>
              <CredentialProperty label={`${proposal.manager?.firstName} ${proposal.manager?.lastName} Address`}
                                  valueComponent={(<HashSum hash={proposal.manager?.ethereumAddress}/>)}/>
              <CredentialProperty label={`${proposal.manager?.firstName} ${proposal.manager?.lastName} Signature`}
                                  valueComponent={(<HashSum hash={proposal.authorizationDocument.authDocSign}/>)}/>
              {renderMembersSign()}
            </View>
            <View style={styles.topBorderContainer}>

              <CompletableButton
                text={proxyVerified? "Verified": "Verify"}
                loading={proxyVerifyLoading}
                completed={proxyVerified}
                onPress={() => {
                  onVerifyProxyClick()
                }}
              />
            </View>
          </View>

          {credentials.length > 0 && renderCredentials(credentials)}


          <View style={styles.container}>
            <Text style={[styles.containerTitle, {marginHorizontal: 24, marginVertical: 8}]}>Contract</Text>

            <View style={styles.topBorderContainer}>
              <AttachedFile name={contractFileName}/>
              <View style={{marginTop: 24}}>
                <CompletableButton
                  text={contractVerified ? "Verified" : "Verify"}
                  loading={contractVerifyLoading}
                  completed={contractVerified}
                  onPress={() => {
                    onVerifyContractClick()
                  }}
                />
              </View>
            </View>
          </View>

{/*
          <View style={{marginHorizontal: 16, marginVertical: 24}}>
            <Button
              mode={"contained"}
              uppercase={false}
              color={"#08BEE5"}
              labelStyle={{color: "#FFFFFF", fontSize: 14, letterSpacing: 0.2}}
              onPress={() => {
                onSendClick()
              }}
            >Sign</Button>
          </View>*/}
        </View>

      </ScrollView>

    </SafeAreaView>
  )

}

const styles = StyleSheet.create({

  container: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 8,
  },

  title: {
    fontSize: 20,
    color: "#000000",
  },

  description: {
    fontSize: 14,
    color: "#212B36",
  },

  detailsContainer: {
    padding: 16,
  },

  votesContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },

  containerTitle: {
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },

  topBorderContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },
});

export default VerifierProposalDetailsScreen
