import React, {useEffect, useState} from 'react'
import {RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ApiService from "../../api/ApiService";
import IssueStatus from "../../component/IssueStatus";
import * as ProposalType from "../../models/ProposalType";
import PersonPreviewSmall from "../../component/PersonPreviewSmall";
import dateFormat from "dateformat";
import {ProgressBar} from "react-native-paper";

const VerifierProposalsListScreen = ({navigation, route}) => {


  const [proposals, setProposals] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(async () => {
    setLoading(true)
    await reloadProposals()
    setLoading(false)
  }, [])

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    await reloadProposals()
    setRefreshing(false);
  }, []);

  async function reloadProposals() {
    setProposals(await ApiService.getVerifierProposals())
  }

  function renderProposals() {
    return proposals.map(p => renderItem({item: p}))
  }

  function renderItem({item}) {
    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => {
          navigation.navigate("ProposalDetails", {
            proposal: item
          })
        }}>
          <>
            <View style={{flexDirection: "row"}}>
              <View style={{flex: 1, alignItems: "flex-start"}}>
                <Text>{"#" + item.id}</Text>
              </View>
              <View style={{flex: 1, alignItems: "flex-end"}}>
                <IssueStatus status={item.status}/>
              </View>
            </View>

            <View style={styles.infoRow}>
              <View style={{flex: 1, alignItems: "flex-start"}}>
                <Text style={styles.label}>TYPE:</Text>
              </View>
              <View style={{flex: 1, alignItems: "flex-end"}}>
                <Text style={styles.dateValue}>{ProposalType.getDisplayName(item.type)}</Text>
              </View>
            </View>

            {item.manager != null && (
              <View style={styles.infoRow}>
                <View style={{flex: 1, alignItems: "flex-start"}}>
                  <Text style={styles.label}>MANAGER:</Text>
                </View>
                <View style={{flex: 1, alignItems: "flex-end"}}>
                  <PersonPreviewSmall
                    firstName={item.manager.firstName}
                    lastName={item.manager.lastName}
                    photo={item.manager.photo}
                  />
                </View>
              </View>
            )}

            <View style={styles.infoRow}>
              <View style={{flex: 1, alignItems: "flex-start"}}>
                <Text style={styles.label}>PROPOSAL DATE:</Text>
              </View>
              <View style={{flex: 1, alignItems: "flex-end"}}>
                <Text style={styles.dateValue}>{dateFormat(item.createdAt, "yyyy-mm-dd")}</Text>
              </View>
            </View>
          </>
        </TouchableOpacity>

      </View>
    );
  }

  return (
    <SafeAreaView>
      <ProgressBar color={"#08BEE5"} visible={loading}/>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>

        <View>
          <Text style={{
            marginHorizontal: 16,
            marginTop: 32,
            marginBottom: 12,
            fontSize: 20
          }}>Verifier Proposals</Text>
        </View>

        {renderProposals()}

      </ScrollView>
    </SafeAreaView>
  );

}


const styles = StyleSheet.create({
  card: {
    backgroundColor: "#FFFFFF",
    borderRadius: 6,
    elevation: 4,
    padding: 16,
    marginHorizontal: 16,
    marginVertical: 8,
  },

  type: {
    fontSize: 16,
    color: "#212B36",
  },

  infoRow: {
    flexDirection: "row",
    marginTop: 12,
  },

  label: {
    fontSize: 12,
    color: "#637381",
  },

  dateValue: {
    fontSize: 14,
    color: "#212B36",
  },
});
export default VerifierProposalsListScreen
