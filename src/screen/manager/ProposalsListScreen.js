import React, {useEffect, useState} from "react";
import {
  FlatList,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import IssueStatus from "../../component/IssueStatus";
import PersonPreviewSmall from "../../component/PersonPreviewSmall";
import dateFormat from "dateformat";
import { FAB } from "react-native-paper";
import { AffinityHelper } from "../../affinity/Affinity";
import ApiService from "../../api/ApiService";
import {web3} from "../../tools/web3";
import Icon from "react-native-vector-icons/MaterialIcons";
import * as ProposalType from '../../models/ProposalType'


const ProposalsListScreen = ({navigation}) => {

  const account = web3.eth.accounts.create();
  const [signedProposals, setSignedProposals] = useState([])
  const [refreshing, setRefreshing] = useState(false)

  useEffect(async () => {
    setSignedProposals(await ApiService.getManagerSignedProposals())
  }, [])

  function renderItem({ item }) {
    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => {
          navigation.navigate("ProposalDetails", {
            proposal: item
          })
        }}>
          <>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1, alignItems: "flex-start" }}>
                <Text>{"#" + item.id}</Text>
              </View>
              <View style={{ flex: 1, alignItems: "flex-end" }}>
                <IssueStatus status={item.status} />
              </View>
            </View>

            <View style={styles.infoRow}>
              <View style={{ flex: 1, alignItems: "flex-start" }}>
                <Text style={styles.label}>TYPE:</Text>
              </View>
              <View style={{ flex: 1, alignItems: "flex-end" }}>
                <Text style={styles.dateValue}>{ProposalType.getDisplayName(item.type)}</Text>
              </View>
            </View>

            <View style={styles.infoRow}>
              <View style={{ flex: 1, alignItems: "flex-start" }}>
                <Text style={styles.label}>PROPOSAL DATE:</Text>
              </View>
              <View style={{ flex: 1, alignItems: "flex-end" }}>
                <Text style={styles.dateValue}>{dateFormat(item.createdAt, "yyyy-dd-mm")}</Text>
              </View>
            </View>
          </>
        </TouchableOpacity>

      </View>
    );
  }

  async function onRefresh() {
    setSignedProposals(await ApiService.getManagerSignedProposals())

  }

  const proposalsList = signedProposals.map(item => renderItem({item: item}))

  const emptyListView = (
    <View style={[styles.container]}><Text>Proposals list is empty</Text></View>
  )

  return (
      <View style={{flex: 1, flexDirection: "column"}}>
        <ScrollView
          contentContainerStyle={{flex: 1}}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }>

          <Text style={{
            marginHorizontal: 16,
            marginTop: 32,
            marginBottom: 12,
            fontSize: 20
          }}>Manager Proposals List</Text>

            {signedProposals.length > 0 ? proposalsList : emptyListView}
        </ScrollView>

        <FAB
          style={styles.fab}
          icon={({ size, color }) => (
            <Icon name="add" color={"#FFFFFF"} size={24} />
          )}          onPress={() => {
            navigation.navigate("CreateProposal");
          }}
        />
      </View>
  )

};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection:"column",
    marginVertical: 16,
    marginHorizontal: 16,
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
  },

  fab: {
    position: 'absolute',
    margin: 24,
    right: 0,
    bottom: 0,
    backgroundColor: "#08BEE5"
  },

  scanFab: {
    position: 'absolute',
    marginHorizontal: 24,
    marginBottom: 100,
    right: 0,
    bottom: 0,
    backgroundColor: "#08BEE5"
  },

  card: {
    backgroundColor: "#FFFFFF",
    borderRadius: 6,
    elevation: 4,
    padding: 16,
    marginHorizontal: 16,
    marginVertical: 8,
  },

  infoRow: {
    flexDirection: "row",
    marginTop: 12,
  },
  dateValue: {
    fontSize: 14,
    color: "#212B36",
  },
})

export default ProposalsListScreen;
