import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import {ProgressBar} from "react-native-paper";
import {AffinityHelper, EmploymentCredentialType, IDCredentialType} from "../../affinity/Affinity";
import {loadWallet} from "../../tools/WalletSecureStorage";
import {ManagerNavContext} from "../../navigation/ManagerNavigator";
import ApiService from "../../api/ApiService";
import Localstorage from "../../tools/localstorage";


const SyncScreen = () => {
  const [syncError, setSyncError] = React.useState(null);
  const {syncCompleted, syncFailed} = React.useContext(ManagerNavContext);

  React.useEffect(() => {
    sync();
  }, []);

  const onRetryClick = () => {
    setSyncError(null);
    sync();
  };

  async function checkAndCreateVC() {
    const wallet = await loadWallet();
    const profile = await Localstorage.getProfile();
    const ethAddress = wallet.address;


    if (!await AffinityHelper.hasEthWalletCredential(ethAddress)) {
      await AffinityHelper.createEthWalletVC(ethAddress);
    }

    if (!await AffinityHelper.hasCredentialWithType(IDCredentialType)) {
      await AffinityHelper.createIDCredential({
        credentialData: {
          firstName: profile.firstName,
          lastName: profile.lastName,
          citizenship: "BY",
          dateOfBirth: "1970-01-01T00:00:00.001Z",
          personalDate: "111222333444",
          documentNumber: "AS1234567",
          expirationDate: "2031-01-01T00:00:00.001Z",
        },
        expirationDate: new Date("2031-01-01T00:00:00.001Z"),
      });
    }

    if (!await AffinityHelper.hasCredentialWithType(EmploymentCredentialType)) {

      await AffinityHelper.createEmploymentCredential({
        credentialData: {
          employer: "Acme Corp",
          position: "Manager",
          startDate: "2001-07-20:00:00:001Z",
          endDate: null,
        },
        expirationDate: new Date("2026-08-09T00:00:00.001Z"),
      });
    }

    const credentials = await AffinityHelper.getCredentials()
    await ApiService.managerPushCredentials(credentials)
    const apiCreds = await ApiService.getCredentials()
    console.log(JSON.stringify(apiCreds))
  }


  const renderError = () => {
    return (
      <View style={styles.errorContainer}>
        <Icon name="sync-problem" size={56} color="#FFFFFF"/>
        <Text style={styles.errorTitle}>Sync Failed</Text>
        <Text style={styles.errorMessage}>Reason: ERROR</Text>
        <TouchableOpacity onPress={onRetryClick}>
          <Text style={styles.retryButton}>Retry</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const sync = async () => {
    try {
      await checkAndCreateVC();
      syncCompleted();
    } catch (e) {
      console.error(JSON.stringify(e))
      syncFailed();
      setSyncError(e);
    }
  };

  return (
    <View style={{
      flex: 1,
      backgroundColor: "#2CB6E1",
      justifyContent: "center",
      alignItems: "center",
    }}>
      {syncError != null ? renderError() : (
        <ProgressBar color={'white'} visible={true} indeterminate={true} style={{width: 200}}/>)}
    </View>
  );
};

const styles = StyleSheet.create({
  errorContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  errorTitle: {
    color: "#FFFFFF",
    fontFamily: "Poppins-Bold",
    fontSize: 18,
  },
  errorMessage: {
    color: "#FFFFFF",
    fontFamily: "Poppins-Regular",
    fontSize: 12,
    marginTop: 8,
  },
  retryButton: {
    color: "#FFFFFF",
    fontFamily: "Poppins-Medium",
    fontSize: 14,
    marginTop: 32,
  },
});

export default SyncScreen;
