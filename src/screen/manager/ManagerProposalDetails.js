import React, {useEffect, useState} from "react";
import {Image, SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import {Appbar, Avatar, Button} from "react-native-paper";
import {IconVotingPassed} from "../../component/Icons";
import {VotingStatus} from "../../models/VotingStatus";
import Countdown from "../../component/Countdown";
import Icon from "react-native-vector-icons/MaterialIcons";
import dateFormat from "dateformat";
import VotesBlock from "../../component/VotesBlock";
import VoteParam from "../../component/VoteParam";
import CredentialProperty from "../../component/CredentialProperty";
import AttachedFile from "../../component/AttachedFile";
import ApiService, {BASE_URL} from "../../api/ApiService";
import CompletableButton from "../../component/CompletableButton";
import PersonPreviewSmall from "../../component/PersonPreviewSmall";
import HashSum from "../../component/HashSum";
import {EmploymentCredentialType, IDCredentialType} from "../../affinity/Affinity";
import IssueCredential from "../../component/IssueCredential";
import EmploymentCredential from "../../component/EmploymentCredential";
import IdCredential from "../../component/IdCredential";
import {DocumentDirectoryPath, downloadFile} from "react-native-fs";
import RnHash, {CONSTANTS} from "react-native-hash";
import {web3} from "../../tools/web3";
import {loadWallet} from "../../tools/WalletSecureStorage";

const ManagerProposalDetails = ({navigation, route}) => {

  const proposal = route.params.proposal;
  const proxyFileName = proposal.authorizationDocument.documentURL.split('/').pop()
  const contractFileName = proposal.additionalDocument.documentURL.split('/').pop()
  console.log(JSON.stringify(proposal, null, 2))
  const [voting, setVoting] = useState();
  const [votingObject, setVotingObject] = useState()
  const [credentials, setCredentials] = useState([])
  const [contractSign, setContractSign] = useState(null)
  const [sendLoading, setSendLoading] = useState(false)

  let valitTillDate = new Date(proposal.issue.createdAt);
  switch (proposal.validity) {
    case  "1 month": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+1));
      break;
    }
    case  "6 month": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+6));
    }
    case  "1 year": {
      valitTillDate =  new Date(valitTillDate.setMonth(valitTillDate.getMonth()+12));
    }
  }

  useEffect(() => {
    reloadVoting()
    reloadCredentials()

  }, [])

  async function reloadVoting() {
    try {
      const voting = await ApiService.managerGetVoting(proposal.issue.id)
      // console.log(voting)
      setVotingObject({
        participants: voting.countPossibleVoice,
        yes: voting.countVoiceYes,
        no: voting.countVoiceNo,
        abstain: voting.countVoiceAbstain,
      })
      setVoting(voting)
    } catch (e) {
      alert(JSON.stringify(e))
      console.error(JSON.stringify(e))
    }
  }

  async function reloadCredentials() {
    setCredentials(proposal.verifiedCredentialDocuments)
  }

  async function onSignContractClick() {
    const walletData = await loadWallet()
    const account = web3.eth.accounts.privateKeyToAccount(walletData.privateKey)

    const url = BASE_URL + "/" + proposal.additionalDocument.documentURL;
    const parts = url.split('/');
    const fileName = parts[parts.length - 1];
    const path = `${DocumentDirectoryPath}/${fileName}`;
    const options = {
      fromUrl: url,
      toFile: path
    }
    console.log("download options", options)
    const response = await downloadFile(options);

    await response.promise;
    const hash = await RnHash.hashFile(path, CONSTANTS.HashAlgorithms.md5)
    console.log("hash", hash);
    setContractSign(account.sign(hash))
  }

  async function onSendClick() {
    try {
      if (contractSign == null) {
        alert("Contract is not signed")
        return
      }
      setSendLoading(true)
      await ApiService.managerSendContract(
        proposal.issue.id,
        proposal.id,
        contractSign.signature
      )
      setSendLoading(false)
      navigation.goBack();
    }catch (e) {
      console.error("send error:", JSON.stringify(e))
      alert(JSON.stringify(e))
    }
  }






  function renderCredentials(credentials) {
    let result = []
    for (let cred of credentials) {

      if (cred.documentJSON.type.includes(EmploymentCredentialType)) {
        console.log(cred)
        result.push(<EmploymentCredential btnVisible={false} credential={cred} />)
      } else if (cred.documentJSON.type.includes(IDCredentialType)) {
        result.push(<IdCredential credential={cred} btnVisible={false}/>)
      }
    }

    return result
  }






  return (
    <SafeAreaView>

      <Appbar
        dark={false}
        style={{backgroundColor: "#FFFFFF"}}
      >
        <Appbar.BackAction onPress={() => {
          navigation.goBack()
        }}/>
        <Appbar.Content title="Proposal"/>
      </Appbar>

      <ScrollView>

        <View style={{marginBottom: 56}}>
          <View style={styles.container}>

            <View style={styles.detailsContainer}>

              <View style={{flexDirection: "row"}}>
                <View style={{flex: 10}}>
                  <Text style={styles.title}>{proposal.issue.title}</Text>
                </View>
                <View style={{flex: 2, flexDirection: "row", alignItems: "center"}}>
                  <IconVotingPassed color={"#2CC68F"}/>
                  <Text style={{color: "#2CC68F"}}>Passed</Text>
                </View>
              </View>

              <View
                style={{flexDirection: "row", marginVertical: 12, justifyContent: "flex-start", alignItems: "center"}}>
                {voting != null && (
                  <>
                    <Icon name={"schedule"} size={16} color={"#8FA4B5"} style={{marginEnd: 12}}/>
                    <Text style={{color: "#637381"}}>{dateFormat(new Date(voting.endDate), "yyyy-dd-mm")}</Text>
                  </>
                )}
              </View>

              <Text>{proposal.issue.description}</Text>
            </View>

            <View style={styles.votesContainer}>
              <VotesBlock
                titleStyle={styles.containerTitle} votingObject={votingObject}
              />
            </View>
          </View>


          <View style={styles.container}>
            <Text style={[styles.containerTitle, {marginHorizontal: 24, marginVertical: 8}]}>Contract</Text>

            <View style={styles.topBorderContainer}>
              <AttachedFile name={contractFileName}/>
              <View style={{marginTop: 24}}>
                <CompletableButton
                  text={contractSign != null ? "Signed": "Sign"}
                  loading={false}
                  completed={contractSign != null}
                  onPress={() => {
                    onSignContractClick()
                  }}
                />
              </View>
            </View>
          </View>


          <View style={styles.container}>
            <Text style={[styles.containerTitle, {marginHorizontal: 24, marginVertical: 8}]}>Proxy</Text>

            <View style={[styles.topBorderContainer, {flexDirection: "row", alignItems: "center"}]}>
              <View style={{flex: 2}}>
                <Image source={require("../../../assets/img/company_logo.png")} style={{width: 64, height: 64}}/>
              </View>
              <View style={{flex: 10}}>
                <Text style={{fontSize: 22, marginStart: 16}}>Acme Corp</Text>
              </View>
            </View>

            <View style={styles.topBorderContainer}>
              <CredentialProperty label="Authorization Document" valueComponent={(<AttachedFile name={proxyFileName}/>)}/>
              <CredentialProperty label="Issue date"
                                  valueText={dateFormat(new Date(proposal.createdAt), "yyyy-dd-mm")}/>
              <CredentialProperty label="Valit till"
                                  valueText={dateFormat(new Date(valitTillDate), "yyyy-dd-mm")}/>
              <CredentialProperty label="Issued to"
                                  valueText={"Hello"}
                                  valueComponent={(
                <PersonPreviewSmall
                  firstName={proposal.manager?.firstName}
                  lastName={proposal.manager?.lastName}
                  photo={null}/>)}
              />
              <CredentialProperty label={"Authorization Document Signature"}
                                  valueComponent={(<HashSum hash={proposal.authorizationDocument.authDocSign}/>)}/>
            </View>
          </View>


          {credentials.length > 0 && renderCredentials(credentials)}


          <View style={{marginHorizontal: 16, marginVertical: 24}}>
            <Button
              mode={"contained"}
              uppercase={false}
              color={"#08BEE5"}
              labelStyle={{color: "#FFFFFF", fontSize: 14, letterSpacing: 0.2}}
              onPress={() => {
                onSendClick()
              }}
            >Send</Button>
          </View>


        </View>
      </ScrollView>

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 8,
  },

  title: {
    fontSize: 20,
    color: "#000000",
  },

  description: {
    fontSize: 14,
    color: "#212B36",
  },

  detailsContainer: {
    padding: 16,
  },

  votesContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },

  containerTitle: {
    color: "#637381",
    fontSize: 12,
    textTransform: "uppercase",
  },

  topBorderContainer: {
    borderTopWidth: 1,
    borderTopColor: "#DDE4E9",
    padding: 16,
  },
});

export default ManagerProposalDetails;
