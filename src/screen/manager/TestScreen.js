import React, { useEffect, useState } from "react";
import { Image, Text, View } from "react-native";
import { Button } from "react-native-paper";
import { AffinityWallet } from "@affinidi/wallet-react-native-sdk";
import { AffinityAccount, AffinityHelper, options } from "../../affinity/Affinity";
import Localstorage from "../../tools/localstorage";

const { CommonNetworkMember } = require("@affinidi/wallet-core-sdk");
import { VCSPhonePersonV1, getVCPhonePersonV1Context } from "@affinidi/vc-data";


const TestScreen = ({ navigation }) => {

  useEffect(async () => {
    const data = await Localstorage.getAffinityAccount();
    AffinityHelper.init(data._password, data._encryptedSeed)
  }, []);

  const onCreateDidClick = async () => {
    await AffinityHelper.verifyCredential()
  };

  return (
    <View>
      <Text>Test Screen</Text>

      <Button
        mode={"contained"}
        onPress={() => onCreateDidClick()}
      >Create DID</Button>

    </View>
  );
};

export default TestScreen;
