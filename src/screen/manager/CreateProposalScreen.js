import React, { useEffect, useState } from "react";
import { SafeAreaView, ScrollView, StyleSheet, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { Appbar, Button, Text, TextInput } from "react-native-paper";
import { TextInputTheme } from "../../themes/AppTheme";
import Icon from "react-native-vector-icons/MaterialIcons";
import RequiredCredential from "../../component/RequiredCredential";
import AvailableCredential from "../../component/AvailableCredential";
import ChooseFileButton from "../../component/ChooseFileButton";
import RnHash, { CONSTANTS } from "react-native-hash";
import { loadWallet } from "../../tools/WalletSecureStorage";
import { web3 } from "../../tools/web3";
import CompletableButton from "../../component/CompletableButton";
import {
  AffinityHelper,
  EmploymentCredentialType,
  ETHWalletCredentialType,
  IDCredentialType,
} from "../../affinity/Affinity";
import ProvidedCredential from "../../component/ProvidedCredential";
import { CredentialUtils } from "../../tools/CredentialUtils";
import ApiService from "../../api/ApiService";
const RNGRP = require('react-native-get-real-path');
import * as RNFS from 'react-native-fs'



const CreateProposalScreen = ({navigation}) => {

  const [type, setType] = useState("PURCHASE_CONTRACT_PROXY")
  const [description, setDescription] = useState("")
  const [validity, setValidity] = useState(null)
  const [authDoc, setAuthDoc] = useState(null);
  const [authDocSign, setAuthDocSign] = useState()
  const [additionalDoc, setAdditionalDoc] = useState(null);
  const [additionalDocSign, setAdditionalDocSign] = useState(null);
  const [availableCredentials, setAvailableCredentials] = useState([])
  const [providedCredentials, setProvidedCredentials] = useState([])
  const [createLoading, setCreateLoading] = useState(false)
  useEffect(async () => {
    setAvailableCredentials(await ApiService.getCredentials())
  }, [] )


  function renderRequiredCredentials() {
    const requiredCreds = [
      {
        name: "Employment credential",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        isProvided: providedCredentials.filter((cred) => cred.documentJSON.type.includes(EmploymentCredentialType)).length > 0,
      },
      {
        name: "ID credential",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        isProvided: providedCredentials.filter((cred) => cred.documentJSON.type.includes(IDCredentialType)).length > 0,
      },
    ];

    return (
      <>
        <View style={{ flexDirection: "row", marginHorizontal: 16 }}>
          <Text style={{ color: "#212B36", fontSize: 18 }}>Required Credentials </Text>
          <Text style={{ color: "#8FA4B5", fontSize: 18 }}>({requiredCreds.length})</Text>
        </View>

        <View style={styles.divider} />

        <View style={styles.credentialsBlock}>
          {requiredCreds.map(item => (
            <RequiredCredential
              name={item.name}
              description={item.description}
              isProvided={item.isProvided}
            />
          ))}
        </View>
      </>
    );
  }

  function renderAvailableCredentials() {

    const availalbeCreds = availableCredentials.filter((cred) => {
      for (let provided of providedCredentials) {
        if (provided.id === cred.id) return false
      }
      return true
    })

    return (
      <>
        <View style={{ flexDirection: "row", marginStart: 16, marginTop: 16 }}>
          <Text style={{ color: "#212B36", fontSize: 18 }}>Available Credentials </Text>
          <Text style={{ color: "#8FA4B5", fontSize: 18 }}>({availalbeCreds.length})</Text>
        </View>

        <View style={styles.divider} />

        <View style={styles.credentialsBlock}>
          {availalbeCreds.map(item => (
            <AvailableCredential
              name={CredentialUtils.getCredentialDisplayName(item.documentJSON)}
              isRequired={CredentialUtils.isCredentialRequired(item.documentJSON, type)}
              onAddPress={() => {
                let tmp = JSON.parse(JSON.stringify(providedCredentials))
                tmp.push(item)
                setProvidedCredentials(tmp)
              }} />
          ))}
        </View>
      </>
    );
  }

  function renderProvidedCredentials() {

    return providedCredentials.length > 0 && (
      <>
        <View style={{ flexDirection: "row", marginStart: 16, marginTop: 16 }}>
          <Text style={{ color: "#212B36", fontSize: 18 }}>Provided Credentials </Text>
          <Text style={{ color: "#8FA4B5", fontSize: 18 }}>({providedCredentials.length})</Text>
        </View>

        <View style={styles.divider} />

        <View style={styles.credentialsBlock}>
          {providedCredentials.map(item => (
            <ProvidedCredential
              name={CredentialUtils.getCredentialDisplayName(item.documentJSON)}
              isRequired={CredentialUtils.isCredentialRequired(item.documentJSON, type)}
              onDeleteClick={() => {
                let tmp = JSON.parse(JSON.stringify(providedCredentials))
                for (let index = 0; index < tmp.length; index++) {
                  if (tmp[index].id === item.id) {
                    tmp.splice(index, 1);
                    setProvidedCredentials(tmp)
                    break;
                  }
                }
              }} />
          ))}
        </View>
      </>
    );
  }

  //TODO: need implementation for ios
  async function onSignDocsClick() {
    console.log("onSignDocsClick")
    console.log("authDoc", authDoc)
    console.log("additionalDoc", additionalDoc)
    const authDoHash = await RnHash.hashFile(authDoc.fileCopyUri, CONSTANTS.HashAlgorithms.md5)
    console.log("authDoHash", authDoHash)
    const additionalDocHash = await RnHash.hashFile(additionalDoc.fileCopyUri, CONSTANTS.HashAlgorithms.md5)
    console.log("additionalDocHash", additionalDocHash)

    const data = await loadWallet();
    const account = web3.eth.accounts.privateKeyToAccount(data.privateKey)
    setAuthDocSign(account.sign(authDoHash))
    setAdditionalDocSign(account.sign(additionalDocHash))
  }

  function renderSignDocsButton() {

    const signed = additionalDocSign != null && authDocSign != null
    return (
      <CompletableButton
        onPress={() => onSignDocsClick()}
        text={ signed ? "Signed" : "Sign"}
        completed={signed}
        style={styles.signBtn}
      />
    )
  }

  async function onCreateClick() {
    try {
      setCreateLoading(true)
      const data = {
        type: type,
        description: description,
        validity: validity,
        authDocSign: authDocSign.signature,
        additionalDocSign: additionalDocSign.signature,
        providedCredentials: JSON.stringify(providedCredentials.map(item => item.id))
      }
      await ApiService.createProposal({
        data: data,
        authDoc: authDoc,
        additionalDoc: additionalDoc
      })
      setCreateLoading(false)
      navigation.goBack()
    } catch (e) {
      console.error(e)
      alert(JSON.stringify(e))
    }
    finally {
      setCreateLoading(false)
    }
  }

  return (
    <SafeAreaView>

      <Appbar
        dark={false}
        style={{ backgroundColor: "#FFFFFF" }}
      >
        <Appbar.BackAction onPress={() => {
          navigation.goBack()
        }} />
        <Appbar.Content title="Create Proposal" />

      </Appbar>
      <ScrollView>
        <View style={styles.container}>

          <View style={styles.block}>

            <View style={styles.subBlock}>
              <DropDownPicker
                items={[
                  { label: "Purchase Contract Proxy", value: "1" },
                  { label: "Bank Account Opening Proxy", value: "2", hidden: true },
                  { label: "Broker Service Proxy", value: "3", hidden: true },
                ]}
                defaultValue={"1"}
                containerStyle={{ height: 40 }}
                style={{ backgroundColor: "#FFFFFF" }}
                itemStyle={{
                  justifyContent: "flex-start",
                }}
                dropDownStyle={{ backgroundColor: "#FFFFFF" }}
                onChangeItem={item => console.log(item)}
              />


              <View style={{marginTop: 12}}>
                <DropDownPicker
                  items={[
                    { label: "1 month", value: "1 month"},
                    { label: "6 month", value: "6 month"},
                    { label: "1 year", value: "1 year"},
                  ]}
                  placeholder={"Validity"}
                  placeholderStyle={{color: "#637381"}}
                  containerStyle={{ height: 40 }}
                  style={{ backgroundColor: "#FFFFFF" }}
                  itemStyle={{
                    justifyContent: "flex-start",
                  }}
                  dropDownStyle={{ backgroundColor: "#FFFFFF" }}
                  onChangeItem={item => setValidity(item.value)}
                />

              </View>
              <TextInput
                mode="flat"
                placeholder="Description"
                style={styles.descriptionInput}
                multiline={true}
                underlineColor="transparent"
                numberOfLines={5}
                onChange={(event) => {
                  setDescription(event.nativeEvent.text);
                }}
                theme={TextInputTheme}
              />
            </View>

            <View style={styles.divider} />

            <View style={styles.subBlock}>
              <View style={{ flexDirection: "column" }}>
                <Text style={styles.label}>Authorization Document</Text>
                <ChooseFileButton
                  filename={authDoc?.name}
                  onChanged={(file) => {
                    setAuthDoc(file);
                    setAuthDocSign(null)
                  }}
                  style={{ marginTop: 12 }}
                />
              </View>

              <View style={{ flexDirection: "column", marginTop: 16 }}>
                <Text style={styles.label}>Additional Document</Text>
                <ChooseFileButton
                  filename={additionalDoc?.name}
                  onChanged={(file) => {
                    setAdditionalDoc(file)
                    setAdditionalDocSign(null)
                  }}
                  style={{ marginTop: 12 }}
                />
              </View>

              <View style={{ flexDirection: "row", marginTop: 24 }}>
                {renderSignDocsButton()}
              </View>
            </View>

            <View style={styles.divider} />

            {renderRequiredCredentials()}

          </View>


          <View style={styles.block}>
            {renderProvidedCredentials()}
          </View>

          <View style={styles.block}>
            {renderAvailableCredentials()}
          </View>

          <Button
            mode={"contained"}
            style={{ marginVertical: 32, marginHorizontal: 16 }}
            color={"#08BEE5"}
            labelStyle={{ color: "#FFFFFF" }}
            uppercase={false}
            loading={createLoading}
            onPress={() => onCreateClick()}
          >Create</Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingBottom: 56,
  },

  block: {
    marginHorizontal: 12,
    marginVertical: 16,
    backgroundColor: "#FFFFFF",
    borderColor: "#DDE4E9",
    borderRadius: 4,
    borderWidth: 1,
  },

  subBlock: {
    marginHorizontal: 16,
    marginVertical: 16,
  },


  credentialsBlock: {
    marginHorizontal: 16,
    paddingBottom: 32,
  },

  descriptionInput: {
    marginTop: 12,
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: "#FFFFFF",
  },

  chooseFileBtn: {
    elevation: 4,
    marginTop: 8,
  },

  signBtn: {
    elevation: 4,
    marginTop: 8,
  },

  label: {
    color: "#637381",
    textTransform: "uppercase",
  },

  divider: {
    height: 1,
    backgroundColor: "#DDE4E9",
    marginVertical: 16,
  },

});

export default CreateProposalScreen;
