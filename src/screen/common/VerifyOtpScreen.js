import React, { useRef, useState } from "react";
import { Dimensions, SafeAreaView, StyleSheet, View } from "react-native";
import { Button, TextInput } from "react-native-paper";
import AnimateLoadingButton from "react-native-animate-loading-button";
import { TextInputTheme } from "../../themes/AppTheme";
import { AffinityWallet } from "@affinidi/wallet-react-native-sdk";
import { AuthContext } from "../../navigation/AppNavigator";
import Localstorage from "../../tools/localstorage";
import { options } from "../../affinity/Affinity";


const VerifyOtpScreen = ({ navigation, route }) => {

  const token = route.params.token;
  const verifyUpBtnRef = useRef();
  const [confirmationCode, setConfirmationCode] = useState();

  const { signIn } = React.useContext(AuthContext);

  async function onVerifyClick() {
    try {
      verifyUpBtnRef.current.showLoading(true);

      const account = route.params.authType === "signup"
        ? await verifySignUp(confirmationCode)
        : await verifySignIn(confirmationCode);

      await Localstorage.setAffinityAccount(account);

      verifyUpBtnRef.current.showLoading(false);
      // signIn({ token: account._encryptedSeed, role: "manager" });
      navigation.navigate("AddWallet")
    } catch (e) {
      verifyUpBtnRef.current.showLoading(false);
      console.error(e);
    }
  }

  async function resendOtp() {
    await AffinityWallet.resendSignUpConfirmationCode(route.params.username, options)
  }

  async function verifySignUp(confirmationCode) {
    const options = {
      env: "prod",
      issueSignupCredential: true,
      apiKey: "b010102e-e2b7-4b22-9762-724fa0c27190",
    };
    return await AffinityWallet.confirmSignUp(token, confirmationCode, options);
  }

  async function verifySignIn(confirmationCode) {
    const options = {
      env: "prod",
      apiKey: "b010102e-e2b7-4b22-9762-724fa0c27190",
    };
    const { isNew, commonNetworkMember } =  await AffinityWallet.confirmSignIn(token, confirmationCode, options);
    return commonNetworkMember;
  }

  return (
    <SafeAreaView style={styles.container}>

      <View style={{ flexDirection: "row" }}>
        <TextInput
          mode={"flat"}
          placeholder={"Confirmation code"}
          style={styles.seedInput}
          theme={TextInputTheme}
          onChange={(event) => {
            setConfirmationCode(event.nativeEvent.text);
          }} />
      </View>

      <View style={{ flexDirection: "row" }}>
        <Button
          mode={"outline"}
          onPress={()=> {}}
        >Resend</Button>
      </View>

      <View style={{
        position: "absolute",
        bottom: 24,
      }}>
        <AnimateLoadingButton
          ref={verifyUpBtnRef}
          width={Dimensions.get("window").width - 24}
          height={48}
          title="Verify"
          titleColor="#FFFFFF"
          backgroundColor="#08BEE5"
          borderRadius={16}
          onPress={onVerifyClick.bind(this)}
        />
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "#F9FAFC",
    justifyContent: "center", // vertical
    alignItems: "center", // horizontal
  },

  seedInput: {
    flex: 1,
    marginStart: 16,
    marginEnd: 16,
    textAlignVertical: "top",

    borderRadius: 4,
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDE4E9",
  },

  button: {
    backgroundColor: "#08BEE5",
    borderRadius: 12,
  },


});

export default VerifyOtpScreen;
