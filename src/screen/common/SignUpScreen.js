import React, { useRef, useState } from "react";
import { Dimensions, SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Text, TextInput } from "react-native-paper";
import AnimateLoadingButton from "react-native-animate-loading-button";
import { TextInputTheme } from "../../themes/AppTheme";
import { AffinityWallet } from "@affinidi/wallet-react-native-sdk";

const SignUpScreen = ({ navigation }) => {

  const signUpBtnRef = useRef();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const htmlMessage = `
  <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
    <tr>
     <td bgcolor="#70bbd9">
       here is your {{CODE}}.
     </td>
    </tr>
  </table>
`;
  const messageParameters = {
    message: "Welcome to Affinity, your OTP: {{CODE}}",
    subject: "Your verification Code",
    htmlMessage: htmlMessage,
  };


  async function onSignUpClick() {
    try {
      signUpBtnRef.current.showLoading(true);
      const token = await AffinityWallet.signUp(username, password, {
        storageRegion: "SGP",
        env: "prod",
        apiKey: "b010102e-e2b7-4b22-9762-724fa0c27190",
        // apiKeyHash: "b0479f84ba0add51ed12b964c536d59fbfacc000861e31f854a546708af6ba58"
      }, messageParameters);
      signUpBtnRef.current.showLoading(false);
      navigation.navigate("VerifyOtp", {
        token: token,
        authType: "signup"
      });
    } catch (e) {
      console.error(e);
      signUpBtnRef.current.showLoading(false);
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          mode={"flat"}
          placeholder={"Username"}
          style={styles.seedInput}
          theme={TextInputTheme}
          onChange={(event) => {
            setUsername(event.nativeEvent.text);
          }} />
      </View>

      <View style={{ flexDirection: "row" }}>
        <TextInput
          mode={"flat"}
          placeholder={"Password"}
          style={styles.seedInput}
          theme={TextInputTheme}
          onChange={(event) => {
            setPassword(event.nativeEvent.text);
          }} />
      </View>

      <TouchableOpacity onPress={() => {
        navigation.navigate("SignIn")
      }}>
        <View>
          <Text>Already have an account?</Text>
        </View>
      </TouchableOpacity>

      <View style={{
        position: "absolute",
        bottom: 24,
      }}>
        <AnimateLoadingButton
          ref={signUpBtnRef}
          width={Dimensions.get("window").width - 24}
          height={48}
          title="SignUp"
          titleColor="#FFFFFF"
          backgroundColor="#08BEE5"
          borderRadius={16}
          onPress={onSignUpClick.bind(this)}
        />
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "#F9FAFC",
    justifyContent: "center", // vertical
    alignItems: "center", // horizontal
  },

  seedInput: {
    flex: 1,
    marginStart: 16,
    marginEnd: 16,
    textAlignVertical: "top",

    borderRadius: 4,
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDE4E9",
  },

  button: {
    backgroundColor: "#08BEE5",
    borderRadius: 12,
  },


});

export default SignUpScreen;
