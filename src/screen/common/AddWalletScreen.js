import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, SafeAreaView, TextInput } from "react-native";
import { TextInputTheme } from "../../themes/AppTheme";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Button } from "react-native-paper";
import { web3 } from "../../tools/web3";
import * as Keychain from "react-native-keychain";
import * as WalletSecureStorage from '../../tools/WalletSecureStorage'
import { AffinityHelper, EmploymentCredentialType, IDCredentialType } from "../../affinity/Affinity";
import { AuthContext } from "../../navigation/AppNavigator";
import ApiService from "../../api/ApiService";
import Localstorage from "../../tools/localstorage";
import QRCodeScanner from "react-native-qrcode-scanner";
import {RNCamera} from "react-native-camera";


const AddWalletScreen = ({ navigation }) => {

  const [privateKey, setPrivateKey] = useState("");
  const [biometryType, setBiometryType] = useState();
  const [createLoading, setCreateLoading] = useState(false)
  const [importLoading, setImportLoading] = useState(false)

  useEffect(() => {
    Keychain.getSupportedBiometryType({}).then((type) => {
      setBiometryType(type);
    });
  }, []);

  const { signIn } = React.useContext(AuthContext);

/*  async function onCreateClick() {
    try {
      setCreateLoading(true)
      const account = web3.eth.accounts.create();
      console.log("Create Result = ", account);
      await WalletSecureStorage.saveWallet({
        address: account.address,
        privateKey: account.privateKey
      })
      setCreateLoading(false)

      navigation.navigate("WalletCreated", {
        address: account.address,
        privateKey: account.privateKey
      })
    } catch (e) {
      setCreateLoading(false)
      console.error(e)
    }
  }*/

  async function onImportClick() {
    try {
      setImportLoading(true)
      const account = web3.eth.accounts.privateKeyToAccount(privateKey);

      await WalletSecureStorage.saveWallet({
        address: account.address,
        privateKey: account.privateKey
      })

      console.log(account)
      await Localstorage.setEthAddress(account.address)

      const signInResponse = await ApiService.signIn(privateKey)
      console.log("SIGN IN RESPONSE:" , JSON.stringify(signInResponse))
      await Localstorage.setProfile(signInResponse)
      // await AffinityHelper.deleteCredential()
      setImportLoading(false)

      signIn({token: signInResponse.accessToken, role: signInResponse.userRole})
    } catch (e) {
      setImportLoading(false)
      alert(JSON.stringify(e))
      console.error(JSON.stringify(e))
    }
  }


  return (
    <View style={{ flex: 1, flexDirection: "column"}}>

      <View style={{ flex: 8, justifyContent: "center" }}>
        <QRCodeScanner
          reactivate={privateKey.length=== 0}
          onRead={(e) => {
            console.log(e.data)
            setPrivateKey(e.data)
          }}
          flashMode={RNCamera.Constants.FlashMode.off}
        />
      </View>
      <View style={{ flex: 4, justifyContent: "center", padding:24}}>

        <TextInput
          mode="flat"
          placeholder="Private Key"
          style={styles.input}
          multiline={true}
          underlineColor="transparent"
          numberOfLines={5}
          onChange={(event) => {
            setPrivateKey(event.nativeEvent.text);
          }}
          value={privateKey}
          theme={TextInputTheme}
        />

        <Button
          mode={"contained"}
          loading={importLoading}
          compact={true}
          uppercase={false}
          color={"#FFFFFF"}
          style={{
            marginTop: 24,
          }}
          onPress={() => {
            onImportClick();
          }}
          disabled={importLoading}
        >Import</Button>
      </View>

{/*      <View style={{
        flex: 6,
        justifyContent: "center",
        borderTopColor: "#DDE4E9",
        borderTopWidth: 1,
      }}>

        <Text style={{ color: "#212B36", fontSize: 16, textAlign: "center" }}>
          If you do not have a private key, you can create a new wallet by clicking "Create"
        </Text>

        <Button
          mode={"contained"}
          loading={createLoading}
          compact={true}
          uppercase={false}
          color={"#08BEE5"}
          labelStyle={{ color: "white" }}
          style={{ marginTop: 32 }}
          onPress={() => {
            onCreateClick();
          }}
          disabled={createLoading}
        >Create</Button>
      </View>*/}

    </View>
  );
};

const styles = StyleSheet.create({


  input: {
    marginTop: 12,
    borderColor: "#DDE4E9",
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: "#FFFFFF",
  },
});

export default AddWalletScreen;
