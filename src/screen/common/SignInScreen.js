import React, { useRef, useState } from "react";
import { Dimensions, SafeAreaView, StyleSheet, View } from "react-native";
import { TextInput } from "react-native-paper";
import AnimateLoadingButton from "react-native-animate-loading-button";
import { TextInputTheme } from "../../themes/AppTheme";
import { AffinityWallet } from "@affinidi/wallet-react-native-sdk";
import { AffinityHelper, options } from "../../affinity/Affinity";
import Localstorage from "../../tools/localstorage";
import { AuthContext } from "../../navigation/AppNavigator";

const htmlMessage = `
  <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
    <tr>
     <td bgcolor="#70bbd9">
       here is your {{CODE}}.
     </td>
    </tr>
  </table>
`;
const messageParameters = {
  message: "Welcome to Affinity, your OTP: {{CODE}}",
  subject: "Your verification Code",
  htmlMessage: htmlMessage,
};


const SignInScreen = ({ navigation }) => {

  const signinBtnRef = useRef();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();



  async function onSignInClick() {
    try {
      signinBtnRef.current.showLoading(true);
      if (password == null || password.length === 0) {
        const token = await AffinityWallet.passwordlessLogin(username, options, messageParameters);
        signinBtnRef.current.showLoading(false);

        navigation.navigate("VerifyOtp", {
          token: token,
          authType: "signin",
        });
      } else {
        const wallet = await AffinityHelper.signIn(username, password)
        const credentials = await AffinityHelper.getCredentials();

        await Localstorage.setAffinityAccount(wallet);
        signinBtnRef.current.showLoading(false);
        navigation.navigate("AddWallet")
      }
    } catch (e) {
      signinBtnRef.current.showLoading(false);
      console.error(e);
    }
  }


  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          mode={"flat"}
          placeholder={"Username"}
          style={styles.seedInput}
          theme={TextInputTheme}
          autoCapitalize='none'
          onChange={(event) => {
            setUsername(event.nativeEvent.text);
          }} />
      </View>

      <View style={{ flexDirection: "row" }}>
        <TextInput
          mode={"flat"}
          placeholder={"Password"}
          style={styles.seedInput}
          theme={TextInputTheme}
          secureTextEntry={true}
          onChange={(event) => {
            setPassword(event.nativeEvent.text);
          }} />
      </View>
      <View style={{
        position: "absolute",
        bottom: 24,
      }}>
        <AnimateLoadingButton
          ref={signinBtnRef}
          width={Dimensions.get("window").width - 24}
          height={48}
          title="Sign In"
          titleColor="#FFFFFF"
          backgroundColor="#08BEE5"
          borderRadius={16}
          onPress={onSignInClick.bind(this)}
        />
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "#F9FAFC",
    justifyContent: "center", // vertical
    alignItems: "center", // horizontal
  },

  seedInput: {
    flex: 1,
    marginStart: 16,
    marginEnd: 16,
    textAlignVertical: "top",
    borderRadius: 4,
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDE4E9",
  },

  button: {
    backgroundColor: "#08BEE5",
    borderRadius: 12,
  },


});

export default SignInScreen;
