import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Button, TouchableRipple } from "react-native-paper";
import Clipboard from '@react-native-community/clipboard'
import { AffinityHelper } from "../../affinity/Affinity";
import Localstorage from "../../tools/localstorage";
import { AuthContext } from "../../navigation/AppNavigator";



const WalletCreatedScreen = ({navigation, route}) => {
  const [createVcLoading, setCreateVcLoading] = useState(false)
  const { signIn } = React.useContext(AuthContext);


  function onCopyAddressClick () {
    Clipboard.setString(route.params.address)
  }

  function onCopyPrivateKeyClick () {
    Clipboard.setString(route.params.privateKey)
  }

  async function onCreateVCClick() {
    try {
      setCreateVcLoading(true)
      const data = await Localstorage.getAffinityAccount();
      AffinityHelper.init(data._password, data._encryptedSeed)
      const vc = await AffinityHelper.createEthWalletVC(route.params.address)
      setCreateVcLoading(false)
      signIn({token:"accessToken", role: "manager"})
    } catch (e) {
      console.error(e)
      setCreateVcLoading(false)
    }
  }

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center",}}>

      <View style={styles.block}>
        <View style={{ flexDirection: "row", alignItems: "center"}}>
          <Text style={{ fontSize: 18 }}>Your address:</Text>
          <TouchableRipple style={styles.copyButton} onPress={() => {onCopyAddressClick()}}>
            <Icon size={24} name="content-copy" />
          </TouchableRipple>
        </View>
        <View style={styles.privateKeyBlock}>
          <Text style={styles.privateKeyStyle} selectable>{route.params.address}</Text>
        </View>
      </View>

      <View style={styles.block}>
        <View style={{ flexDirection: "row", alignItems: "center"}}>
          <Text style={{ fontSize: 18 }}>Your private key:</Text>
          <TouchableRipple style={styles.copyButton} onPress={() => {onCopyPrivateKeyClick()}}>
            <Icon size={24} name="content-copy" />
          </TouchableRipple>
        </View>

        <View style={styles.privateKeyBlock}>
          <Text style={styles.privateKeyStyle} selectable>{route.params.privateKey}</Text>
        </View>
      </View>


      <Button
        mode={"contained"}
        uppercase={false}
        color={"#08BEE5"}
        loading={createVcLoading}
        labelStyle={{ color: "white" }}
        style={{ position: "absolute",bottom: 32}}
        disabled={createVcLoading}
        onPress={() => {
          onCreateVCClick()
        }}
      >Create VC</Button>
    </View>
  );

};

const styles = StyleSheet.create({

  block: {
    marginHorizontal: 24,
    marginVertical: 12,
    justifyContent: "center",
    alignItems: "center",
  },

  privateKeyBlock: {
    marginTop: 12,
    padding: 12,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#DDE4E9",
    justifyContent: "center",
    alignItems: "center",
  },

  privateKeyStyle: {},

  copyButton: {
    marginStart: 24,
    padding: 8
  },

});

export default WalletCreatedScreen;
