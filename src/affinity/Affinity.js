import {AffinityWallet} from "@affinidi/wallet-react-native-sdk";
import {VCSPhonePersonV1} from "@affinidi/vc-data";
import Localstorage from "../tools/localstorage";

const {CommonNetworkMember} = require('@affinidi/wallet-core-sdk')

export const options = {
  storageRegion: "SGP",
  env: "prod",
  apiKey: "7de2c3e4-1a3a-402a-bd6a-c4f5d01f5e82",
  // apiKeyHash: "ae1accf857eaa0882d99a778bb9dfbcf080baad3d6926b197d617a2e14e25555"
};

export const ETHWalletCredentialType = "EthWalletVC";
const EthWalletVCName = "ETH Wallet";
export const IDCredentialType = "IDCredential";
const IDCredentialName = "ID Credential";
export const EmploymentCredentialType = "EmploymentCredential";
const EmploymentCredentialName = "Employment Credential";

export const AffinityHelper = {

  initAsync: async function () {
    const data = await Localstorage.getAffinityAccount();
    this.init(data._password, data._encryptedSeed);
  },

  init: function (password, encryptedSeed) {
    // console.log("init", password, encryptedSeed);
    this._password = password;
    this._encryptedSeed = encryptedSeed;
    this.wallet = new AffinityWallet(this._password, this._encryptedSeed, options);
  },

  signIn: async function (username, password) {
    this.wallet = await AffinityWallet.fromLoginAndPassword(username, password, options);
    return this.wallet;
  },

  createEthWalletVC: async function (publicKey) {
    await this.checkWallet();

    // console.log("createEthWalletCVC", JSON.stringify(this.wallet, null, 2));

    const requesterDid = this.wallet.did;
    // console.log("requesterDid", requesterDid);
    const credentialOfferRequestToken = await this.wallet.generateCredentialOfferRequestToken([{
      type: ETHWalletCredentialType,
    }]);
    // console.log("credentialOfferRequestToken", credentialOfferRequestToken);

    const credentialOfferResponseToken = await this.wallet.createCredentialOfferResponseToken(credentialOfferRequestToken);
    // console.log("credentialOfferResponseToken", credentialOfferResponseToken);

    const credentialSubject: VCSPhonePersonV1 = {
      data: {
        "@type": ["Person", "PersonE", "PersonEthWallet"],
        publicKey: publicKey,
      },
    };

    const credentialMetadata = {
      context: [{
        "@version": 1.1,
        "data": {
          "@id": "https://docs.affinity-project.org/issuer-util/vc/context/index.html#data",
          "@type": "@json",
        },
      },
      ],
      name: EthWalletVCName,
      type: [ETHWalletCredentialType],
    };

    const signedCredential = await this.wallet.signCredential(
      credentialSubject,
      credentialMetadata,
      {credentialOfferResponseToken, requesterDid},
      new Date(new Date().getTime() + 365 * 24 * 60 * 60 * 1000).toISOString(),
    );

    await this.saveCredentials([signedCredential]);
    return signedCredential;
  },

  /**
   *
   * @param credentialData object {firstName: string, lastName: string, citizenship:String, dateOfBirth: string, personalDate:string, documentNumber: string, expirationDate: string}
   * @param expirationDate Date credential expiration date
   * @return VCV1
   *
   * */

  createIDCredential: async function ({credentialData, expirationDate}) {
    const signedCredential = await this.createSignedCredential(
      {
        name: IDCredentialName,
        type: IDCredentialType,
        data: credentialData,
        expirationDate: expirationDate,
      },
    );
    await this.saveCredentials([signedCredential]);
    return signedCredential;
  },

  /**
   *
   * @param credentialData object {employer: string, positionId: string, startDate:String, endDate: string}
   * @param expirationDate Date credential expiration date
   * @return VCV1
   *
   * */
  createEmploymentCredential: async function ({credentialData, expirationDate}) {
    const signedCredential = await this.createSignedCredential(
      {
        name: EmploymentCredentialName,
        type: EmploymentCredentialType,
        data: credentialData,
        expirationDate: expirationDate,
      },
    );
    await this.saveCredentials([signedCredential]);
    return signedCredential;
  },


  saveCredentials: async function (credentialsArr) {
    await this.checkWallet();

    await this.wallet.saveCredentials(credentialsArr, options.storageRegion);
  },

  getCredentials: async function () {
    await this.checkWallet();

    return await this.wallet.getCredentials();
  },

  hasEthWalletCredential: async function (publicKey) {
    await this.checkWallet();

    const credentials = await this.wallet.getCredentials();
    for (let cred of credentials) {

      if (
        typeof cred === "object"
        && cred.type?.includes(ETHWalletCredentialType)
        && cred.credentialSubject.data.publicKey === publicKey
      ) return true;
    }
    return false;
  },

  hasCredentialWithType: async function (type) {
    await this.checkWallet();
    const credentials = await this.wallet.getCredentials();
    for (let cred of credentials) {

      if (
        typeof cred === "object"
        && cred.type?.includes(type)
      ) return true;
    }
    return false;

  },

  checkWallet: async function () {
    if (this.wallet == null) await this.initAsync();
  },

  createSignedCredential: async function ({name, type, data, expirationDate}) {
    await this.checkWallet();

    const requesterDid = this.wallet.did;
    const credentialOfferRequestToken = await this.wallet.generateCredentialOfferRequestToken([{
      type: type,
    }]);

    const credentialOfferResponseToken = await this.wallet.createCredentialOfferResponseToken(credentialOfferRequestToken);

    const credentialSubject: VCSPhonePersonV1 = {
      data: {
        "@type": ["Person", "PersonE"],
        ...data,
      },
    };

    const credentialMetadata = {
      context: [{
        "@version": 1.1,
        "data": {
          "@id": "https://docs.affinity-project.org/issuer-util/vc/context/index.html#data",
          "@type": "@json",
        },
      },
      ],
      name: name,
      type: [type],
    };

    return await this.wallet.signCredential(
      credentialSubject,
      credentialMetadata,
      {credentialOfferResponseToken, requesterDid},
      expirationDate?.toISOString(),
    );
  },


  getShareCredentialsRequest: async function (reqirements, issuerDid) {
    await this.checkWallet();
    const token = await this.wallet.generatePresentationChallenge(
      reqirements,
    );
    return token
  },

  getShareCredentialsResponse: async function (reqToken) {
    await this.checkWallet();
    const credentials = await this.getCredentials()
    const token = await this.wallet.createCredentialShareResponseToken(
      reqToken,
      credentials,
    )
    return token
  },


  verifyCredential: async function (resToken) {
    await this.checkWallet()
      const {isValid, did, nonce, suppliedCredentials} = await this.wallet.verifyCredentialShareResponseToken(
        resToken,
      );

      console.log(`isValid = ${isValid}\ndid=${did}\nnonce=${nonce}\nsuppliedCredentials=${suppliedCredentials}`);
      return isValid;
  },

};

