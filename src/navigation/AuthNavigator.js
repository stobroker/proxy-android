import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import SignUpScreen from "../screen/common/SignUpScreen";
import VerifyOtpScreen from "../screen/common/VerifyOtpScreen";
import SignInScreen from "../screen/common/SignInScreen";
import AddWalletScreen from "../screen/common/AddWalletScreen";
import WalletCreatedScreen from "../screen/common/WalletCreatedScreen";

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      headerMode="none"
    >
{/*      <Stack.Screen name="SignUp"
                    component={SignUpScreen}
                    options={{ title: "Sign Up" }}
      />*/}
      <Stack.Screen name="SignIn"
                    component={SignInScreen}
                    options={{ title: "Sign In" }}
      />
      <Stack.Screen name="WalletCreated"
                    component={WalletCreatedScreen}
                    options={{ title: "Add wallet" }}
      />
      <Stack.Screen name="AddWallet"
                    component={AddWalletScreen}
                    options={{ title: "Add wallet" }}
      />

      <Stack.Screen name="VerifyOtp"
                    component={VerifyOtpScreen}
                    options={{ title: "OTP" }}
      />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
