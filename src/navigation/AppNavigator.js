import React, {useEffect, useReducer} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Localstorage from "../tools/localstorage";
import SplashScreen from "../screen/SplashScreen";
import AuthNavigator from "./AuthNavigator";
import ManagerNavigator from "./ManagerNavigator";
import BoardMemberNavigator from "./BoardMemberNavigator";
import VerifierNavigator from "./VerifierNavigator";


const Stack = createStackNavigator();

export const AuthContext = React.createContext();

const splashNavigator = (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="Splash" component={SplashScreen}/>
  </Stack.Navigator>
);

const AppNavigator = () => {
  const [state, dispatch] = useReducer((prevState, action) => {
    switch (action.type) {
      case 'RESTORE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          userRole: action.role,
          isLoading: false,
        };
      case 'SIGN_IN':
        return {
          ...prevState,
          isSignout: false,
          userToken: action.token,
          userRole: action.role
        };
      case 'SIGN_OUT':
        return {
          ...prevState,
          isSignout: true,
          userToken: null,
          userRole: null,
        };
    }
  }, {
    isLoading: true,
    isSignout: false,
    userToken: null,
    userRole: null,
  })

  useEffect(() => {
    const bootstrapAsync = async () => {
      try {
        const accessToken = await Localstorage.getAccessToken();
        const role = await Localstorage.getRole();
        dispatch({type: 'RESTORE_TOKEN', token: accessToken, role: role});
      } catch (e) {
        // Restoring token failed
        dispatch({type: 'RESTORE_TOKEN', token: null, role: null});

      }
    };

    bootstrapAsync();
  }, [])


  const authContext = React.useMemo(
    () => ({
      signIn: async data => {
        await Localstorage.setAccessToken(data.token)
        await Localstorage.setRole(data.role)
        dispatch({type: 'SIGN_IN', token: data.token, role: data.role});
      },
      signOut: () => dispatch({type: 'SIGN_OUT'}),
      signUp: async data => {
        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
    }),
    []
  );

  const getNavigator = (state) => {
    if (state.isLoading) {
      return splashNavigator
    } else if (state.userToken == null || state.userRole == null) {
      return (<AuthNavigator/>)
    } else if (state.userRole === "manager") {
      return (<ManagerNavigator/>)
    } else if (state.userRole === "boardMember") {
      return (<BoardMemberNavigator/>)
    } else if (state.userRole === "verifier") {
      return (<VerifierNavigator/>)
    } else {
      throw `Role ${state.userRole} not implemented`
    }
  }

  return (
    <AuthContext.Provider value={authContext}>
      {getNavigator(state)}
    </AuthContext.Provider>
  );
};

export default AppNavigator;
