import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from "react-native-vector-icons/MaterialIcons";
import BoardMemberShareVcRequestsScreen from "../screen/member/BoardMemberShareVcRequestsScreen";
import BoardMemberIssuesListScreen from "../screen/member/BoardMemberIssuesListScreen";

const Tab = createBottomTabNavigator();

const BoardMemberBottomNavigator = () => {

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={"ProposalsList"}
        component={BoardMemberIssuesListScreen}
        options={{
          tabBarLabel: 'Proposals',
          tabBarIcon: ({focused, color, size}) => (
            <Icon name={"list"} size={size} color={color}/>
          ),
        }}/>
      <Tab.Screen
        name={"ShareVcRequests"}
        component={BoardMemberShareVcRequestsScreen}
        options={{
          tabBarLabel: 'Requests',
          tabBarIcon: ({focused, color, size}) => (
            <Icon name={"account-circle"} size={size} color={color}/>
          )
        }}/>
    </Tab.Navigator>
  )

}

export default BoardMemberBottomNavigator;
