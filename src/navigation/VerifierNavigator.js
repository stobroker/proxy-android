import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import VerifierProposalsListScreen from "../screen/verifier/VerifierProposalsListScreen";
import VerifierProposalDetailsScreen from "../screen/verifier/VerifierProposalDetailsScreen";


const Stack = createStackNavigator();

const VerifierNavigator = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="ProposalsList"
                    component={VerifierProposalsListScreen}/>
      <Stack.Screen name="ProposalDetails"
                    component={VerifierProposalDetailsScreen}/>



    </Stack.Navigator>
  )
}

export default VerifierNavigator;
