import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import BoardMemberIssuesListScreen from "../screen/member/BoardMemberIssuesListScreen";
import BoardMemberVotingScreen from "../screen/member/BoardMemberVotingScreen";
import BoardMemberCreateIssueScreen from "../screen/member/BoardMemberCreateIssueScreen";
import BoardMemberProposalDetailsScreen from "../screen/member/BoardMemberProposalDetailsScreen";
import BoardMemberSyncScreen from "../screen/member/BoardMemberSyncScreen";
import BoardMemberBottomNavigator from "./BoardMemberBottomNavigator";

const Stack = createStackNavigator();
export const BoardMemberNavContext = React.createContext();


const BoardMemberNavigator = () => {


  const [state, dispatch] = React.useReducer((prevState, action) => {
    switch (action.type) {
      case "SYNC_FAILED": {
        return {
          ...prevState,
          isSynced: false,
        };
      }
      case "SYNC_SUCCESS": {
        return {
          ...prevState,
          isSynced: true,
        };
      }
      case "CHANGE_STATUSBAR_COLOR": {
        return {
          ...prevState,
          statusBarColor: action.color,
        };
      }
    }
  }, {
    isSynced: false,
    statusBarColor: "#2CB6E1",
  });

  const boardMemberNavContext = React.useMemo(
    () => ({
      syncFailed: async data => {
        dispatch({type: "SYNC_FAILED", error: data.error});
      },
      syncCompleted: async data => {
        dispatch({type: "SYNC_SUCCESS"});
      },
      setStatusbarColor: async data => {
        if (data.color.toString() !== state.statusBarColor.toString()) {
          dispatch({type: "CHANGE_STATUSBAR_COLOR", color: data.color});
        }
      },
    }),
    [],
  );


  function getNavigator(state) {
    return (
      <Stack.Navigator
        headerMode="none"
        screenOptions={{animationEnabled: false}}
        mode="modal"
      >
        {
          state.isSynced ?
            (
              <>
                <Stack.Screen name="Home"
                              component={BoardMemberBottomNavigator}/>
                <Stack.Screen name="ProposalDetails"
                              component={BoardMemberProposalDetailsScreen}/>
                <Stack.Screen name="CreateIssue"
                              component={BoardMemberCreateIssueScreen}/>
                <Stack.Screen name="VotingDetails"
                              component={BoardMemberVotingScreen}/>
              </>
            ) : (
              <>
                <Stack.Screen name="Sync" component={BoardMemberSyncScreen}/>
              </>
            )
        }
      </Stack.Navigator>
    );
  }

  return (
    <BoardMemberNavContext.Provider value={boardMemberNavContext}>
      {getNavigator(state)}
    </BoardMemberNavContext.Provider>
  )
}

export default BoardMemberNavigator;
