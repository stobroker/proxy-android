import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import CreateProposalScreen from "../screen/manager/CreateProposalScreen";
import SyncScreen from "../screen/manager/SyncScreen";
import CreateShareVCResponseTokenScreen from "../screen/CreateShareVCResponseTokenScreen";
import ManagerBottomNavigator from "./ManagerBottomNavigator";
import ManagerProposalDetails from "../screen/manager/ManagerProposalDetails";

const Stack = createStackNavigator();
export const ManagerNavContext = React.createContext();


const ManagerNavigator = () => {

  const [state, dispatch] = React.useReducer((prevState, action) => {
    switch (action.type) {
      case "SYNC_FAILED": {
        return {
          ...prevState,
          isSynced: false,
        };
      }
      case "SYNC_SUCCESS": {
        return {
          ...prevState,
          isSynced: true,
        };
      }
      case "CHANGE_STATUSBAR_COLOR": {
        return {
          ...prevState,
          statusBarColor: action.color,
        };
      }
    }
  }, {
    isSynced: false,
    statusBarColor: "#2CB6E1",
  });


  const managerNavContext = React.useMemo(
    () => ({
      syncFailed: async data => {
        dispatch({type: "SYNC_FAILED", error: data.error});
      },
      syncCompleted: async data => {
        dispatch({type: "SYNC_SUCCESS"});
      },
      setStatusbarColor: async data => {
        if (data.color.toString() !== state.statusBarColor.toString()) {
          dispatch({type: "CHANGE_STATUSBAR_COLOR", color: data.color});
        }
      },
    }),
    [],
  );

  function getNavigator(state) {
    return (
      <Stack.Navigator
        headerMode="none"
        screenOptions={{animationEnabled: false}}
        mode="modal"
      >
        {
          state.isSynced ?
            (
              <>
                <Stack.Screen name="Home" component={ManagerBottomNavigator}/>
                <Stack.Screen name="ProposalDetails" component={ManagerProposalDetails}/>
                <Stack.Screen name="CreateProposal" component={CreateProposalScreen}/>
                <Stack.Screen name="QRScannerScreen" component={CreateShareVCResponseTokenScreen}/>
              </>
            ) : (
              <>
                <Stack.Screen name="Sync" component={SyncScreen}/>
              </>
            )
        }
      </Stack.Navigator>
    );
  }

  return (
    <ManagerNavContext.Provider value={managerNavContext}>
      {getNavigator(state)}
    </ManagerNavContext.Provider>
  );
};

export default ManagerNavigator;
