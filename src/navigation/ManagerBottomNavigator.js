import * as React from 'react';
import {BottomTabBar, createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ProposalsListScreen from "../screen/manager/ProposalsListScreen";
import ShareVcRequestsScreen from "../screen/manager/ShareVcRequestsScreen";
import Icon from "react-native-vector-icons/MaterialIcons";

const Tab = createBottomTabNavigator();

const ManagerBottomNavigator = () => {

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={"ProposalsList"}
        component={ProposalsListScreen}
        options={{
          tabBarLabel: 'Proposals',
          tabBarIcon: ({focused, color, size}) => (
            <Icon name={"list"} size={size} color={color}/>
          ),
        }}/>
      <Tab.Screen
        name={"ShareVcRequests"}
        component={ShareVcRequestsScreen}
        options={{
          tabBarLabel: 'Requests',
          tabBarIcon: ({focused, color, size}) => (
            <Icon name={"account-circle"} size={size} color={color}/>
          )
        }}/>
    </Tab.Navigator>
  )

}

export default ManagerBottomNavigator;
