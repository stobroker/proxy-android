import {web3} from "../tools/web3";
import Localstorage from "../tools/localstorage";

export const BASE_URL = "http://mind-dev.com:3010"

const NetworkError = new Error({code: 500, message: "NETWORK_ERROR"})
const UndefinedError = new Error({code: null, message: "UNDEFINED_ERROR"})

const parseResponseError = async (response) => {
  try {
    let json = await response.json()
    return json

  } catch (e) {
    return UndefinedError
  }
}
const handleRequest = async (networkCall) => {
  let response
  try {
    response = await networkCall
  } catch (e) {
    console.error("NetworkError", e)
    throw NetworkError
  }

  if (response.ok) {
    return await response.json()
  } else {
    throw await parseResponseError(response)
  }
}

const ApiService = {

  signIn: async function (privateKey) {
    const wallet = web3.eth.accounts.privateKeyToAccount(privateKey);

    const signInResponse = await handleRequest(fetch(`${BASE_URL}/api/v1/auth/message`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ethereumAddress: wallet.address})
    }))

    const signResult = wallet.sign(signInResponse.message)
    console.log(JSON.stringify(signResult))
    const confirmSignInResponse = await handleRequest(fetch(`${BASE_URL}/api/v1/auth/verify`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ethereumAddress: wallet.address, signature: signResult.signature})
    }))
    return confirmSignInResponse
  },

  createProposal: async function ({data, authDoc, additionalDoc}) {
    const accessToken = await Localstorage.getAccessToken();

    let reqBody = new FormData()

    Object.keys(data).forEach(key => {
      reqBody.append(key, data[key])
    });

    reqBody.append("authDoc", authDoc)
    reqBody.append("additionalDoc", additionalDoc)

    const proposal = await handleRequest(fetch(`${BASE_URL}/api/v1/manager/proposal`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${accessToken}`
      },
      body: reqBody
    }))
    return proposal;
  },

  managerPushCredentials: async function (vcArr) {
    const accessToken = await Localstorage.getAccessToken();
    const credentials = await handleRequest(fetch(`${BASE_URL}/api/v1/manager/createOrUpdateVC`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Authorization': `Bearer ${accessToken}`
      },
      body: `${encodeURIComponent("documentsJSON")}=${encodeURIComponent(JSON.stringify(vcArr))}`
    }))
    return credentials;
  },

  boardMemberPushCredentials: async function (vcArr) {
    const accessToken = await Localstorage.getAccessToken();
    const credentials = await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/createOrUpdateVC`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Authorization': `Bearer ${accessToken}`
      },
      body: `${encodeURIComponent("documentsJSON")}=${encodeURIComponent(JSON.stringify(vcArr))}`
    }))
    return credentials;
  },

  getManagerShareVCReq: async function () {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/manager/shareVcRequest`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  getManagerSignedProposals: async function () {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/manager/signedProposals`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  managerShareVC: async function (reqId, resToken) {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/manager/shareVcRequest `, {
      method: 'PUT',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        shareVcRequestId: reqId,
        responseToken: resToken
      })
    }))
  },

  boardMemberProvideVCtoProposal: async function (proposalId, creds) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken);
    const body = JSON.stringify({
      proposalId: parseInt(proposalId),
      providedCredentials: JSON.stringify(creds.map( i=> parseInt(i)))
    })

    // const body = `proposalId=${proposalId}&providedCredentials=${(JSON.stringify(creds))}`
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/proposal/VCDocuments`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },

  getCredentials: async function () {
    const accessToken = await Localstorage.getAccessToken();
    const credentials = await handleRequest(fetch(`${BASE_URL}/api/v1/user/verifiedCredentialDocuments`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Authorization': `Bearer ${accessToken}`
      },
    }))
    return credentials;
  },

  getCredential: async function (type) {
    console.log("get credential" , type)
    const accessToken = await Localstorage.getAccessToken();
    const credentials = await handleRequest(fetch(`${BASE_URL}/api/v1/user/verifiedCredentialDocuments`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Authorization': `Bearer ${accessToken}`
      },
    }))


    for (let cred of credentials) {
       console.log(cred.documentJSON.type)
      if (cred.documentJSON.type.includes(type)) return cred;
    }
    return null;
  },


  getVoting: async function (issueId) {
    const accessToken = await Localstorage.getAccessToken();
    const voting = await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/voting?issueId=${issueId}`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Authorization': `Bearer ${accessToken}`
      },
    }))
    return voting;
  },

  managerGetVoting: async function (issueId) {
    const accessToken = await Localstorage.getAccessToken();
    const voting = await handleRequest(fetch(`${BASE_URL}/api/v1/manager/voting?issueId=${issueId}`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Authorization': `Bearer ${accessToken}`
      },
    }))
    return voting;
  },

  getBoardMemberProposals: async function () {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/proposals`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  getVerifierProposals: async function () {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/verifier/signedProposals`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  getBoardMemberShareVcReq: async function (proposalId) {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/shareVcRequests?proposalId=${proposalId}`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  deleteShareVCReq: async function (reqId) {
    const accessToken = await Localstorage.getAccessToken();

    const response = await handleRequest(fetch(`${BASE_URL}/api/v1/user/shareVcRequest`, {
      method: 'DELETE',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({shareVcRequestId: reqId})
    }))
  },

  boardMemberReqShareVC: async function (proposalId, credentialId, requestToken, responseToken) {
    const accessToken = await Localstorage.getAccessToken();
    let body = {
      proposalId: proposalId,
      credentialId: credentialId,
      requestToken: requestToken,
    }
    const response = await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/shareVcRequest`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify(body)
    }))

    return response;
  },

  markCredAsVerified: async function (proposalId, credentialId) {

    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/verifiedCredentialDocument `, {
      method: 'PUT',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        proposalId: proposalId,
        credentialId: credentialId
      })
    }))
  },

  createIssue: async function (proposalId, title, description, startDate, endDate, support, minApprove) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    const body = JSON.stringify({
      proposalId: proposalId,
      title: title,
      description: description,
      startDate: startDate,
      endDate: endDate,
      supportPercentNeed: support,
      minimalApprovalPercentNeed: minApprove
    })
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/issue`, {
      method: 'POST',
      headers: {
        'Accept': '*!/!*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },

  vote: async function (issueId, voice) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    const body = JSON.stringify({
      issueId: issueId,
      voice: voice,
    })
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/voting`, {
      method: 'POST',
      headers: {
        'Accept': '*!/!*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },


  createProxy: async function (issueId, proposalId) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    const body = JSON.stringify({
      issueId: issueId,
      proposalId: proposalId,
    })
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/signedProposal`, {
      method: 'POST',
      headers: {
        'Accept': '*!/!*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },

  managerSendContract: async function (issueId, proposalId, contractSignature) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    const body = JSON.stringify({
      issueId: issueId,
      proposalId: proposalId,
      contractSignature: contractSignature
    })
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/manager/sendContract`, {
      method: 'POST',
      headers: {
        'Accept': '*!/!*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },

  boardMemberSignProxy: async function (proposalId, signature) {
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    const body = JSON.stringify({
      proposalId: proposalId,
      signature: signature
    })
    console.log(body)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/signature`, {
      method: 'POST',
      headers: {
        'Accept': '*!/!*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: body
    }))
  },


  boardMemberVcRequests: async function() {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/shareVcRequest`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  boardMemberShareVcResponse: async function (shareVcRequestId,responseToken) {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/boardMember/shareVcRequest`, {
      method: 'PUT',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        shareVcRequestId: shareVcRequestId,
        responseToken: responseToken
      })
    }))
  },

  verifierCreateShareVcRequest: async function(proposalId,credentialId,requestToken) {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/verifier/shareVcRequest`, {
      method: 'POST',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        proposalId: proposalId,
        credentialId: credentialId,
        requestToken:requestToken
      })
    }))
  },

  verifierGetProposalShareVcRequests: async function(proposalId) {
    const accessToken = await Localstorage.getAccessToken();
    return await handleRequest(fetch(`${BASE_URL}/api/v1/verifier/shareVcRequests?proposalId=${proposalId}`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  },

  getProposalById: async function(proposalId) {
    console.log(proposalId)
    const accessToken = await Localstorage.getAccessToken();
    console.log("accessToken", accessToken)
    return await handleRequest(fetch(`${BASE_URL}/api/v1/user/proposal?proposalId=${proposalId}`, {
      method: 'GET',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    }))
  }

}

export default ApiService;
