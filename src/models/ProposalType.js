
export const PURCHASE_CONTRACT_PROXY = "PURCHASE_CONTRACT_PROXY"
export function getDisplayName(type) {
    switch (type) {
        case PURCHASE_CONTRACT_PROXY : return "Purchase Contract Proxy";
        default: return "Unknown"
    }
}
