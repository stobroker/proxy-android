

export const VotingStatus = {
  PENDING: "PENDING",
  LAUNCHED: "LAUNCHED",
  COMPLETED: "COMPLETED"

}
