

export const TextInputTheme = {
  roundness: 4,
  colors: {
    placeholder: "#70879C",
    text: "#323B43",
    primary: "transparent",
    underlineColor: "transparent",
    underlineColorCustom: "white",
    background: "#fff",
  },
};
