/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const extraNodeModules = () => {
  let tmp = require('node-libs-browser');
  let result = {};
  for (let key of Object.keys(tmp)) {
    result[key] = tmp[key];
  }
  result.mobileRandomBytes =  require.resolve('@affinidi/wallet-react-native-sdk/mobileRandomBytes');
  result.crypto = require.resolve('react-native-crypto');
  return result;
}

module.exports = {
  resolver: {
    resolverMainFields: ["react-native", "browser", "module", "main"],
    extraNodeModules: extraNodeModules()
  },
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
  },
};
