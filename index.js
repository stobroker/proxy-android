/**
 * @format
 */

import {AppRegistry} from 'react-native';
//hotfix https://github.com/tradle/rn-nodeify/issues/70
import "react-native-get-random-values";
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
