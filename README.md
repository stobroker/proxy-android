## Inspiration
Throughout our life, almost each of us faced the problem of authorizing someone to act on behalf of ourselves. This problem was a huge painpoint especially when we managed corporate enterprises abroad. Banks, brokers, counterparties usually requested to authorize a representative to perform certain actions: bank account opening, contract signing, etc. Moreover, the problem of trust still remained because the proxy could be revoked at any moment. Notary verification was not a perfect solution due to its cost, time consumption, and complexity of dealing with apostille (not all states joined the Hague Convention). 

We called our project "Davér Proxy". In the Belarusian language, the word "davér" means "trust". Trust maintenance for corporate governance is the main goal for this hackathon project.  

## What it does
Thus we decided to tackle this problem with an SSI-based solution. We believe that strong timestamping provided by the Ethereum blockchain will provide the system of trustless verification of authorization act. To verify personal credentials of actors and their connection with the corporation we implement an Ethereum-based SSI module.
The solution has three operational roles: Manager, Board Members, Counterparty. VC Issuer is not present on the interface but participates in the process of credential verification. In short, the Manager seeks the approval of the proxy by Board Members. The proxy authorizes the Manager to sign a contract on behalf of the company with the Counterparty.
In our product flow, we presume that users have created their accounts and issued verifiable credentials via the Affinidi solution. The flow contains the following steps:

**Step 1.** Proposal creation. Firstly, the Manager uploads the proxy text and the contract text. The Manager calculates the md5 hash and signs it with his signature. After that, the Manager selects the proper parameters for the proposal. Then he selects proper verifiable credentials from the list of available VC’s and creates a proposal that is sent to Board Members. Board Members receive the proposal which contains: the proxy, the contract, required VC’s to identify the Manager. 

**Step 2.** Proposal verification. Secondly, one of the Board Members verifies signed documents by calculating the hash and finding the Manager’s public key. Then the system compares the found public key with the received Manager’s public key. The procedure is repeated for all the documents. 

**Step 3.** Manager verification. To verify the Manager’s identity one of the Board Members requests his verifiable credential. The Manager receives the request and approves it. Then the system generates a token and sends it to the Issuer. The Issuer responds with the status of the credentials. The status is sent to the Board Member.

**Step 4.** Voting creation. One of the Board Members creates voting. A Board Member can specify the following parameters:
voting title and description;
voting duration (start date and end date are requested for filling); 
voting support. Support is the relative percentage of Board Members that are required to vote “Yes” for a proposal to be approved;
minimum approval. Minimum approval is the percentage of total Board Members that is required to vote “Yes” on a proposal before it can be approved.

**Step 5.** Board Members’ voting. Board Members vote for the proxy issuance. If the voting ends with a positive result, the voting status is changed to “completed”.

**Step 6.** Each board member signs the proxy document with his signature. The process is similar to previously described for the Manager (see Step 1). As soon as the number of signatures becomes equal to the number of voting participants, the proposal status is changed so that it can be sent to the Manager with the voting results. 

**Step 7.** The proxy with the voting results is sent to the Manager. Before signing the contract Manager can check Board members' verifiable credentials (IDs and employment proofs). The Manager signs the contract and sends it to the Сounterparty, The corresponding contract status (“signed”) is set by the system.

**Step 8.** The Counterparty (Verifier) receives and checks the proposal received from the Manager. As soon as the proxy document is downloaded, he can check the md5 hash and validates Board Members' digital signatures. Thus, the non-reputability of the document itself is checked.

**Step 9.** The Verifier requests Board Members’ verifiable credentials to verify their identities and ensure that they have the power to authorize the Manager to act on behalf of the company. When this request is received by a Board Member, a response token is generated and a verifiable credential verification request with this token in its body is sent to the Issuer. The Issuer’s response contains verifiable credential status (whether it is valid, not revoked, etc).  

**Step 10.** Similar to the previous step, the Verifier requests Manager's verifiable credentials to check his identity and position in the company.

**Step 11.** The Verifier checks the digital signature of the provided contract. The process is similar to the one described in Step 2.

## How we built it
During Hackathon we developed four key elements:
1. Figma UI Prototype. https://www.figma.com/proto/rtsACc4l1TqFMD4XyWu5xM/md.STO.Fund-(Aragon)?node-id=4118%3A38569&scaling=min-zoom&page-id=4019%3A0
2. API Server (Node.js, Express.js, Sequielize, Affinidi SDK, Docker, Swagger UI for API docs). Responsible for authorization and voting, storing cached data.
3. Android App (React Native, web3.js, @affinidi/wallet-core-sdk, @affinidi/vc-data, @affinidi/wallet-react-native-sdk, react-native-keychain, fingerprint scanner for private key encryption). The main user interface of the solution: VC creation and validation, digital document signing, voting, etc.
4. Web Application (React, web3, Aragon UI, Affinidi SDK). The Initial idea was to wire Aragon Voting App and voting results from Aragon smart contract as a basis for the proxy creation, but we are planning to build it later.

First, we made UI Prototype using Figma, to have a reference for all team members and to understand how it could work. After this, we made draft API methods on serverside and Android app in parallel. Few days before submission we realized that we cannot fully implement and connect to Aragon Voting and create voting endpoints for the backend.
Videos and descriptions were made in the final hours before the submission. 

## Challenges we ran into
However, during our work, we struggled with Aragon API’s due to the lack of documentation. Therefore we decided to substitute this Aragon voting app and decided to implement a simple voting process fully on a regular backend.

## Accomplishments that we're proud of
We are proud of the strong work of app devs who managed to break through documentation. Also, we managed to make web interfaces for the solution. We almost connected Aragon Voting smart contracts for getting voting results for VC creation based on it. Our solution will let us use different VC providers wiring it to an Ethereum account (issuing VC for connecting Ethereum address to address inside Affinidi).

## What we learned
1. Good understanding of Affinidi solutions. We’ve learned react-native SDK and web-based, server API methods and basic building blocks for using Affinidi as a VC platform.
2. SSI stack. Hackathon is a good place to put your hands on new tools. We are excited about SSI and VC as a way to deal with user data and this was a great few weeks to test our skills in building the app from scratch.
3. Multi-vendor VC wallet scheme.

## What's next for Daver Proxy
We are planning to develop an extended version of the VC verification process for Board Members and develop Company Agent for VC creation for them from the company.
Further, we are planning to implement the function of proxy revocation. In the proxy VC, we indicated that it can be revoked only by voting Board Members. But we did not manage to provide this function in this prototype iteration. 
There is no doubt that the complex onboarding process hinders the mass adoption of OTC security trading and portfolio management. A set of features that includes Daver Proxy can become a game-changer for the midsize security trading industry. We intend to add this solution to our SSI verification service in the field of security trading. We hope that this type of company representative verification will decrease brokers' customer service costs and will foster the acquisition of new clients. 



How to test.

Developed flow needs coordinated efforts form 4 user accounts.

Mobile Phone Requirements:
1. Android 6+, recommended 10+ (was used in testing and development)
2. Fingerprint Scanner. We used it for private key encoding
3. Screen lock using PIN + fingerprint
 
User Accounts. We have pre-issued VC’s for Board Members and Managers (initial VC’s credential was not our main priority), so for running whole system is better to use our backend API and this private keys. This also could be done on a backend, but it will overcomplicate already rather complicated user flow for testing solution.

```
address:0xc1fdc15ec3f0C2c77826027F6661323D95581A49

private key: 0x3bcf6044fd11dc408bf4d231076103323395185a4b944441185f5b0a7ca93a25

role: manager

username: managerHack1

password 9hY7CZat

```

```
address: 0x0CD266881237ed33157C9B7245Bf707e941b6372

private key: 0x63da49bf91554e452ad2182979b65faa839a72e7035b757c765f20e1c249ca0c

role: boardMember

username: boardMemberHack1

password D47zfQrr

```

```
address: 0xE3c1d35cf7581C64FAE9883834585a39907E7283

private key: 0xb260ffc014b87cd9d27af8ba3ec03dbf260b361baee3f6c8e248d2aa80a2acdb

role: boardMember

username: boardMemberHack2

password d6SNG48b

```

```
address: 0x0cf2CcA6d2d687fBCDdB3026A960b08542531325

private key: 0x9fa76daf270f3b355ea35653a2880be4eb73a67ab644157121b6ca180fc62da9

role: verifier

username: verifierHack1

password e8FqJKcJ

```

For logging out from App use should close app and go to  Device Settings -> Apps -> SSIHackaton -> Storage -> Clear Storage


Usage manual ( but it’s recommended to watch the video first for better understanding):
Creating proxy issuance Proposal for manager:
Set proposal type (select “Purchase contract proxy”)
Specify proxy validity
Load authorization document (draft proxy) and additional document (draft agreement) txt files
Sign the documents by pressing the corresponding button. Confirm signing with your fingerprint.
See required verifiable credentials list. Provide each verifiable credential by pressing “plus” button opposite the required credential in the list of avaliable VCs.
Create proposal by pressing the “Create” button at the bottom of the screen.

Proxy issuance:
For any board member account:
Open board members proposals list
Open the proposal received from the Manager
Verify the documents by pressing the corresponding button
Request verifiable credentials from the Manager.
	2)  For Manager’s account:
Provide requested verifiable credentials (go to the “Requests” tab and press check mark on both credentials
3) For any board member account:
Verify provided credentials (press “Verify” button opposite each credential)
Create an issue voting pressing the “Create” button at the bottom of the screen. On the next screen specify voting parameters and press “Create” button
	4) For both board members accounts:
Go to voting screen and press “Yes” button
When voting is passed, both board members have to sign the documents.

5) For any board member account:
When documents are signed, press the “Create proxy” button to send documents to the Manager.

3. Signing and sending documents
1) From Manager’s account:
Go to “Proposals” tab. A proposal with “signed” status appears in the list.
Check board members’ verifiable credentials.
Sign the contract pressing the “Sign” button
Press “Send” button at the bottom of the screen to send documents to the counterparty (the Verifier)

4. …..????
7) From Verifier’s account:
Refresh the screen. A new proposal received from the Manager appears in the list.
Verify the proxy signed by the board members 
Request and verify Manager’s verifiable credentials 



Built with
What languages, frameworks, platforms, cloud services, databases, APIs, or other technologies did you use?
Figma, React Native, NodeJS, Docker, Swagger, ReactJs, Aragon UI, Web3.JS, Affinidi SDK (@affinidi/wallet-core-sdk, @affinidi/vc-data, @affinidi/wallet-react-native-sdk), PostgreSQL, Sequelize, Express, JWT, React Native Keychain

